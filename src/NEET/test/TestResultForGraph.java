/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.test;

/**
 *
 * @author 007
 */
public class TestResultForGraph {
    private int test_Id,subject_Id,total_Questions,correct_Questions;

    public TestResultForGraph(){
        
    }
    
    public TestResultForGraph(int testId,int subject_Id, int total_Questions, int correct_Questions) {
        this.test_Id = testId;
        this.subject_Id = subject_Id;
        this.total_Questions = total_Questions;
        this.correct_Questions = correct_Questions;
    }

    public int getCorrect_Questions() {
        return correct_Questions;
    }

    public void setCorrect_Questions(int correct_Questions) {
        this.correct_Questions = correct_Questions;
    }

    public void setSubject_Id(int subject_Id) {
        this.subject_Id = subject_Id;
    }

    public void setTest_Id(int test_Id) {
        this.test_Id = test_Id;
    }

    public void setTotal_Questions(int total_Questions) {
        this.total_Questions = total_Questions;
    }

    public int getSubject_Id() {
        return subject_Id;
    }

    public int getTest_Id() {
        return test_Id;
    }

    public int getTotal_Questions() {
        return total_Questions;
    }
}