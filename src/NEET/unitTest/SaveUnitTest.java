/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.unitTest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import server.DBConnection1;
import server.Server;
/**
 *
 * @author 007
 */
public class SaveUnitTest {
    public void saveUnitTestBean(UnitTestBean unitTestBean,int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        try
        {  
            int testId=0;
            Statement s=conn.createStatement();
            ResultSet rs = s.executeQuery("Select max(id) from Saved_Test_Info");
                if(rs.next())
                {
                        testId=rs.getInt(1);
                }
            testId++;
            
            String sql="insert into Saved_Test_Info values(?,?,?,?,?)";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            ps.setInt(2, rollNo);
            ps.setInt(3, 3);//TestBean = 1
            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            
            ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
            ObjectOutputStream objOutStream = new ObjectOutputStream(byteOutStream);
            objOutStream.writeObject(unitTestBean);//////unitTest.UnitTestBean
            objOutStream.flush();
            objOutStream.close();
            byteOutStream.close();
            ps.setBytes(5, byteOutStream.toByteArray());
            int i=ps.executeUpdate();            
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public ArrayList<Integer> getSavedTestId(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ArrayList<Integer> ids=new ArrayList<Integer>();
        try{
            String sql="select * from Saved_Test_Info where testType=3 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                int s= rs.getInt(1);
                ids.add(s);
            }
            return ids;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return ids;
    }
    
    public ArrayList<UnitTestBean> retrieveUnitTestBean(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ArrayList<UnitTestBean> unitTestBeans=new ArrayList<UnitTestBean>();
        try
        {  
            String sql="select * from Saved_Test_Info where testType=3 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            if(rs.next())
            {
                UnitTestBean p = new UnitTestBean();
                byte[] data = rs.getBytes(5) ;
                ByteArrayInputStream byteInStream = new ByteArrayInputStream(data);
                ObjectInputStream objInStream = new ObjectInputStream(byteInStream);
                Object obj = objInStream.readObject();
                objInStream.close();
                byteInStream.close();
                if (obj instanceof UnitTestBean) {
                    p = (UnitTestBean)obj;
                }
                unitTestBeans.add(p);
            }
            return unitTestBeans;
        }   
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return null;
    }
    
    public void delUnitTest(int testId)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        try
        {
            String sql="delete from Saved_Test_Info where id=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i=ps.executeUpdate();
            
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public ArrayList<String> getDate(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ArrayList<String> savedTestDate=new ArrayList<String>();
        try{
            String sql="select * from Saved_Test_Info where testType=3 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            while(rs.next())
            {
                String s= rs.getString(4);
                savedTestDate.add(s);
            }
            return savedTestDate;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return savedTestDate;
    }
    
    public Integer isTestPresent(int rollNo)
    {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        try{
            String sql="select * from Saved_Test_Info where testType=3 and rollNo=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, rollNo);
            ResultSet rs=ps.executeQuery();
            if(rs.next()){
                return 1;
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return 0;
    }
    
}