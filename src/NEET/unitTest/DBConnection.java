/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.unitTest;

import java.sql.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.*;
import javax.swing.JOptionPane;
import NEET.question.QuestionBean;
import server.DBConnection1;
import server.Server;
/**
 *
 * @author 007
 */
public class DBConnection {
//    private static String driver = "org.apache.derby.jdbc.EmbeddedDriver";
//    private static String protocol = "jdbc:derby:CETNeetNew;create=true;dataEncryption=true;bootPassword=*007_Worlds+Best+Company@Wagholi_007*";
//    Connection con1,con2,conn;
    ArrayList<Integer> E=new ArrayList<Integer>();
//    Statement s,st1,st2;
//    ResultSet rs,rs1,rs2;
    int chaptercount;
    public DBConnection() {
        
    }    
    
    public UnitTestBean getUnitTestBean(int subjectId,ArrayList<Integer> chapterId) {
        int questionCount=30,totalTime=20;
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            ArrayList<QuestionBean> alQuestions=getQuestionsForUnitTest(subjectId,chapterId,questionCount);
            UnitTestBean unitTestBean=new UnitTestBean();
            unitTestBean.setSubjectId(subjectId);
            unitTestBean.setChapterId(chapterId);
            unitTestBean.setTotalTime(totalTime/2);
            unitTestBean.setRemainingTime(totalTime);
            unitTestBean.setQuestions(alQuestions);
            return unitTestBean;
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public static void delTest(int testId) {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
            String sql="delete from Saved_Test_Info where id=?";
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i=ps.executeUpdate();
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    private ArrayList<QuestionBean> getQuestionsForUnitTest(int subjectId,ArrayList<Integer> chapterIds,int questionCount) {
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();          
            int chapterCount = chapterIds.size();
            int num = questionCount/chapterCount;
            int remaining = questionCount - (chapterCount*num);
            ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
            for(int i=0;i<chapterIds.size();i++){
                int questionPerChapter = num;
                if(i==0){
                    questionPerChapter+=remaining;
                }
                rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id ="+subjectId+" and Chapter_Id ="+chapterIds.get(i)+" order by Question_Id");                              
                ArrayList<QuestionBean> altemp=new ArrayList<QuestionBean>();
                while(rs1.next()){
                    String question,A,B,C,D,Answer,Hint,optionImagePath,QuestionImagePath,hintImagePath;
                    int sub_Id,Chap_Id,Top_Id,question_Id,attempt;
                    question_Id=rs1.getInt(1);
                    question=rs1.getString(2);
                    A=rs1.getString(3);
                    B=rs1.getString(4);
                    C=rs1.getString(5);
                    D=rs1.getString(6);
                    Answer=rs1.getString(7);
                    Hint=rs1.getString(8);                
                    sub_Id=rs1.getInt(9);
                    Chap_Id=rs1.getInt(10);
                    Top_Id=rs1.getInt(11);
                    QuestionImagePath=rs1.getString(13);
                    optionImagePath=rs1.getString(15);
                    hintImagePath=rs1.getString(17);
                    boolean result = (rs1.getInt(12)==0)?false:true;
                    boolean result1 = (rs1.getInt(14)==0)?false:true;               
                    boolean result2 = (rs1.getInt(16)==0)?false:true;               
                    attempt=rs1.getInt(18);
                    QuestionBean q=new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath,result2,hintImagePath,attempt);
                    altemp.add(q);    
                }  
                Collections.shuffle(altemp);
                for(int j=0;j<questionPerChapter;j++){
                    al.add(altemp.get(j));
                }
            }
            return al;
        } catch(Exception e) {
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public void saveStateOfNewPractice(UnitTestBean unitTestBean,int rollNo) {
        int uniITestId=0;
        if(unitTestBean!=null) {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
                rs = s.executeQuery("Select max(unitTestId) from UTest_Info");
                if(rs.next()) {
                    uniITestId=rs.getInt(1);
                }
                uniITestId++;
                java.util.Date date = new java.util.Date();
                SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate=formatter.format(date);
                PreparedStatement ps=conn.prepareStatement("insert into UTest_Info values(?,?,?,?,?,?)");
                ps.setInt(1, uniITestId);
                ps.setInt(2, rollNo);
                ps.setInt(3, unitTestBean.getSubjectId());                
                ps.setString(5,"Started");                
                ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
                ArrayList<Integer> chapterIds = unitTestBean.getChapterId();
                for(int i=0;i<chapterIds.size();i++){
                    ps.setInt(4, chapterIds.get(i));
                    int executeUpdate = ps.executeUpdate();
                }
                unitTestBean.setUnitTestId(uniITestId);
                ArrayList<QuestionBean> alQuestions=unitTestBean.getQuestions();
                Iterator<QuestionBean> it = alQuestions.iterator();
                while(it.hasNext()) {
                    QuestionBean question = it.next();                    
                    ps=conn.prepareStatement("insert into UTest_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, uniITestId);
                    ps.setInt(2, question.getQuestion_Id());
                    ps.setString(3,"unAttempted");
                    ps.executeUpdate();                    
                }
                unitTestBean.setStatus("Started");     
                unitTestBean.setStartDateTime(new java.util.Date());
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
    
    public void setQuestionStatus(QuestionBean question,int unitTestId, String userAnswer)
    {
        if(question!=null) {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
                s.executeUpdate("update UTest_Question_Status_Info set user_Answer='"+userAnswer+"' where Question_Id="+question.getQuestion_Id()+" and unitTestId="+unitTestId);                
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
    
    public ArrayList<Integer> getChapIds(ArrayList<String> nameofchap)
    {
        ArrayList<Integer> noofchap=new ArrayList<Integer>();
        int i=nameofchap.size();
        int Chap_Id;
        for(int m=0;m<i;m++)
        {
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs1;
        try
        {  
            Statement s=conn.createStatement();           
                String name=nameofchap.get(m);
                
                rs1 = s.executeQuery("Select Chapter_Id from Chapter_Info where Chapter_Name ='"+name+"'");          
                while(rs1.next())
                { 
                    Chap_Id=rs1.getInt(1);
                    noofchap.add(Chap_Id);   
                    
                }
            }   
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }    
        return noofchap;
    }  
    
    public int getPerwrongQuestionMarks(int subjectId)
    { 
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();           
            rs = s.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id="+subjectId);           
            if(rs.next())
            {                
                return rs.getInt(1);
            }            
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
    
    public int getPerQuestionMarks(int subjectId)
    { 
        Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
            rs = s.executeQuery("Select * from Subject_Info where Subject_Id="+subjectId);           
            if(rs.next())
            {                
                return rs.getInt(3);
            }            
        }   
        catch(Exception e)
        {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
    
    public String getResult(int unitTestId,int questionId)
    {        
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();          
                rs = s.executeQuery("Select user_Answer from  UTest_Question_Status_Info where unitTestId="+unitTestId+" and Question_Id="+questionId);   
                if(rs.next())
                {
                    return rs.getString(1);
                }                
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
            return null;
    }    
    
    public void addResult(UnitTestResultBean unitTestResultBean,int rollNo) 
    {   
        if(unitTestResultBean!=null)
        {            
            Connection conn=new DBConnection1().getClientConnection2(Server.getServerIP());
        ResultSet rs;
        try
        {  
                     
                String sql="insert into UTest_Result_Info values(?,?,?,?)";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setInt(1,unitTestResultBean.getUnitTsetId());
                ps.setInt(2, rollNo);
                ps.setInt(3,unitTestResultBean.getTotalQuestions());
                ps.setInt(4,unitTestResultBean.getCorrectQuestions());
                
                int r = ps.executeUpdate();
                if(r>0)
                {
                    //System.out.println("Practice Result Added");
                }
                else
                {
                    //System.out.println("Problem in Result Adding");
                }
            } 
            catch (Exception ex) 
            {
                //JOptionPane.showMessageDialog(null, ex.getMessage());
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }

    public void saveStateOfNewTest(UnitTestBean unitTestBean, int rollNo) {
        int testId=0;
        if(unitTestBean!=null)
        {
            Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=conn.createStatement();                 
                java.util.Date date = new java.util.Date();
                SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate=formatter.format(date);
                ArrayList<QuestionBean> alPhysics=unitTestBean.getQuestions();
                              
                Iterator<QuestionBean> it = alPhysics.iterator();
                while(it.hasNext())
                {
                    QuestionBean question = it.next();                    
                    PreparedStatement ps=conn.prepareStatement("insert into Class_Test_Question_Status_Info values(?,?,?,?)");
                    ps.setInt(1, unitTestBean.getUnitTestId());
                    ps.setInt(2, rollNo);
                    ps.setInt(3, question.getQuestion_Id());
                    ps.setString(4,"unAttempted");
                    ps.executeUpdate();
                }  
                unitTestBean.setStartDateTime(new java.util.Date());
            } 
            catch (SQLException ex) 
            {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }

    public void setClassQuestionStatus(QuestionBean question,int unitTestId, String userAnswer) {
        if(question!=null) {
            Connection con=new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try
        {  
            Statement s=con.createStatement();   
                s.executeUpdate("update Class_Test_Question_Status_Info set user_Answer='"+userAnswer+"' where question_Id="+question.getQuestion_Id()+" and test_Id="+unitTestId);                
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        finally{
            if(con!=null){
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        }
    }
}
