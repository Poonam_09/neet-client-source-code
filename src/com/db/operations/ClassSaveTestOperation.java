/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import JEE.unitTest.UnitTestBean;
import com.bean.ClassTestBean;
import com.bean.QuestionBean;
import com.bean.RankBean;
import com.bean.StudentBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.DBConnection1;
import server.Server;

/**
 *
 * @author Aniket
 */
public class ClassSaveTestOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    public int saveNewClassTest(ArrayList<QuestionBean> qId, int subId, int time,String TestName) {
        int testId = 0;
        if (qId != null) {
             conn = new DBConnection1().getClientConnection1(Server.getServerIP());
           
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("Select max(test_Id) from Class_Test_Info");
                if (rs.next()) {
                    testId = rs.getInt(1);
                }
                testId++;
                ps = conn.prepareStatement("insert into Class_Test_Info values(?,?,?,?,?,?)");
                ps.setInt(1, testId);
                ps.setInt(2, subId);
                ps.setString(3, "Not available");
                ps.setTimestamp(4, new java.sql.Timestamp(System.currentTimeMillis()));
                ps.setInt(5, time);
                ps.setString(6,TestName);
                int executeUpdate = ps.executeUpdate();
                if (executeUpdate > 0) {
                    ps = conn.prepareStatement("insert into Class_Test_Question_Info values(?,?,?)");
                    for (int i = 0; i < qId.size(); i++) {
                        ps.setInt(1, testId);
                        ps.setInt(2, qId.get(i).getQuestionId());
                        ps.setInt(3, qId.get(i).getSubjectId());
                        executeUpdate = ps.executeUpdate();
                    }
                    return testId;
                }
            } catch (SQLException ex) {
                Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        return -1;
    }
    
     public void insertintoTest_And_Set(int testId) {
        int ids = 0;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select max(set_Id) from Test_And_Set");
            if (rs.next()) {
                ids = rs.getInt(1);
            }
            ids++;//set_Id INTEGER NOT NULL,test_Id INTEGER NOT NULL,setInitials 
            stmt.execute("insert into Test_And_Set values(" + ids + "," + testId + ",'A','Not Available')");
            ids++;
            stmt.execute("insert into Test_And_Set values(" + ids + "," + testId + ",'B','Not Available')");
            ids++;
            stmt.execute("insert into Test_And_Set values(" + ids + "," + testId + ",'C','Not Available')");
            ids++;
            stmt.execute("insert into Test_And_Set values(" + ids + "," + testId + ",'D','Not Available')");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
     
     public ArrayList<String> getClassTestInfo() {
        ArrayList<String> testInfo = new ArrayList<String>();
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select distinct test_Id ,TEST_NAME ,test_DateTime  from  Class_Test_Info order by test_Id");
            while (rs.next()) {
                String temp = rs.getInt(1) + "\t" + rs.getBoolean(2) + "\t" + rs.getTimestamp(3);
                testInfo.add(temp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testInfo;
    } 
     
     public int getQuestionCount(int testId) {
        conn =new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
             stmt = conn.createStatement();
            rs = stmt.executeQuery("Select count(*) from  Class_Test_Question_Info where testId=" + testId);
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return -1;
    }
     
     public void setTestToPerform(int row) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        ArrayList<Integer> qId = new ArrayList<Integer>();
        try {
            Statement s1 = conn.createStatement();
            Statement s2 = conn.createStatement();
            Statement s3 = conn.createStatement();
            Statement s4 = conn.createStatement();
            Statement s5 = conn.createStatement();
            Statement s6 = conn.createStatement();
            Statement s7 = conn.createStatement();
            Statement s8 = conn.createStatement();
            Statement s9 = conn.createStatement();

            s4.execute("delete from CurrentTestStatusOfStudent");
            s4.execute("delete from Temp_Class_Test_Info");
            s5.execute("delete from Temp_Class_Test_Question_Info");
            rs = s1.executeQuery("Select * from Class_Test_Info where test_Id=" + row);
            if (rs.next()) {
                s2.execute("insert into Temp_Class_Test_Info values (" + rs.getInt(1) + "," + rs.getInt(2) + ",'Available','" + rs.getTimestamp(4) + "'," + rs.getInt(5) + ")");
            }
            rs = s1.executeQuery("Select * from Class_Test_Question_Info where testId=" + row);
            while (rs.next()) {
                qId.add(rs.getInt(2));
            }
            ArrayList<Integer> aidp = new ArrayList<Integer>();
            ArrayList<Integer> aidc = new ArrayList<Integer>();
            ArrayList<Integer> aidb = new ArrayList<Integer>();
            ArrayList<Integer> aidg = new ArrayList<Integer>();
            for (int i = 0; i < qId.size(); i++) {
                // check subject count for multiple sub
                ResultSet resultSet = s6.executeQuery("select Subject_Id from Question_Info where question_Id=" + qId.get(i));
                if (resultSet.next()) {
                    int temp = resultSet.getInt(1);
                    if (temp == 1) {
                        aidp.add(qId.get(i));
                    } else if (temp == 2) {
                        aidc.add(qId.get(i));
                    } else if (temp == 3) {
                        aidb.add(qId.get(i));
                    } else if (temp == 4) {
                        aidg.add(qId.get(i));
                    }
                }
            }



            qId = new ArrayList<Integer>();
            for (int i = 0; i < aidp.size(); i++) {
                qId.add(aidp.get(i));
            }
            for (int i = 0; i < aidc.size(); i++) {
                qId.add(aidc.get(i));
            }
            for (int i = 0; i < aidb.size(); i++) {
                qId.add(aidb.get(i));
            }
            for (int i = 0; i < aidg.size(); i++) {
                qId.add(aidg.get(i));
            }
            char c[];
            String ss = "ABCD";
            c = ss.toCharArray();
            if (qId.size() > 0) {
                String newAlpha = "A";
                for (int i = 0; i < 4; i++) {
                    newAlpha = Character.toString(c[i]);
                    for (int y = 0; y < aidp.size(); y++) {
                        s3.execute("insert into Temp_Class_Test_Question_Info values(" + row + "," + aidp.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidc.size(); y++) {
                        s7.execute("insert into Temp_Class_Test_Question_Info values(" + row + "," + aidc.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidb.size(); y++) {
                        s8.execute("insert into Temp_Class_Test_Question_Info values(" + row + "," + aidb.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidg.size(); y++) {
                        s9.execute("insert into Temp_Class_Test_Question_Info values(" + row + "," + aidg.get(y) + ",'" + newAlpha + "')");
                    }
                    Collections.shuffle(aidp);
                    Collections.shuffle(aidc);
                    Collections.shuffle(aidb);
                    Collections.shuffle(aidg);
                }
            }
            //Update Practice_Info set status='Finished' where Practice_Id=" + practiceId);
            s1.executeUpdate("Update Test_And_Set set status='Available' where test_Id=" + row);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
     
      public ArrayList<Integer> getsubjectIds(int testId) {
        ArrayList<Integer> allSubjects = new ArrayList<Integer>();
        ArrayList<Integer> allquestions = new ArrayList<Integer>();
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            Statement s1 = conn.createStatement();
            Statement s2 = conn.createStatement();
            rs = s1.executeQuery("select distinct question_Id from Class_Test_Question_Info where testId=" + testId);
            while (rs.next()) {
                allquestions.add(rs.getInt(1));
            }
            if (allquestions.size() > 0) {
                for (int i = 0; i < allquestions.size(); i++) {
                    ResultSet rs2 = s2.executeQuery("select distinct subject_Id from Question_Info where Question_Id=" + allquestions.get(i));
                    if (rs2.next()) {
                        if (allSubjects.contains(rs2.getInt(1))) {
                        } else {
                            allSubjects.add(rs2.getInt(1));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return allSubjects;
    }
      
      public int changeStatusOfTest(String string, int flag, int testid) {
        int i = 0;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt= conn.createStatement();
            stmt.executeUpdate("update Class_Test_Info set status='" + string + "' where test_Id=" + testid);
            i = flag;
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return i;
    }
      
      public UnitTestBean getClassTestBean(int testId) {
        UnitTestBean unitTestBean = new UnitTestBean();
        String flag = null;
        ArrayList<Integer> queIds = new ArrayList<Integer>();
        ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt= conn.createStatement();
            rs = stmt.executeQuery("Select status from Class_Test_Info where test_Id=" + testId);
            if (rs.next()) {
                flag = rs.getString(1);
            }
            rs = stmt.executeQuery("Select question_Id from Class_Test_Question_Info where testId=" + testId);
            while (rs.next()) {
                int q = rs.getInt(1);
                queIds.add(q);
            }
            for (int i = 0; i < queIds.size(); i++) {
                rs = stmt.executeQuery("Select * from Question_Info where Question_Id=" + queIds.get(i));
                if (rs.next()) {
                    String question, A, B, C, D, Answer, Hint, optionImagePath, QuestionImagePath, hintImagePath, qyear;
                    int sub_Id, Chap_Id, Top_Id, question_Id, attempt, level, type;
                    question_Id = rs.getInt(1);
                    question = rs.getString(2);
                    A = rs.getString(3);
                    int view = 0;
                    B = rs.getString(4);
                    C = rs.getString(5);
                    D = rs.getString(6);
                    Answer = rs.getString(7);
                    Hint = rs.getString(8);
                    level = rs.getInt(9);
                    sub_Id = rs.getInt(10);
                    Chap_Id = rs.getInt(11);
                    Top_Id = rs.getInt(12);
                    QuestionImagePath = rs.getString(14);
                    optionImagePath = rs.getString(16);
                    hintImagePath = rs.getString(18);
                    boolean result = (rs.getInt(13) == 0) ? false : true;
                    boolean result1 = (rs.getInt(15) == 0) ? false : true;
                    boolean result2 = (rs.getInt(17) == 0) ? false : true;
                    attempt = rs.getInt(19);
                    type = rs.getInt(20);
                    qyear = rs.getString(21);
                    QuestionBean q = new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, level, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath, result2, hintImagePath, attempt, type, qyear, view);
                    al.add(q);
                }
            }
            unitTestBean.setQuestions(al);
            unitTestBean.setStatus(flag);
            return unitTestBean;
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return unitTestBean;
    }
      
//   public UnitTestBean getClassTestBean(int testId) {
//        UnitTestBean unitTestBean = new UnitTestBean();
//        String flag = null;
//        ArrayList<Integer> queIds = new ArrayList<Integer>();
//        ArrayList<QuestionBean> questionBeanList = new ArrayList<QuestionBean>();
//        conn = new DbConnection().getConnection();
//        
//        try {
//            stmt = conn.createStatement();
//            rs = stmt.executeQuery("Select status from Class_Test_Info where test_Id=" + testId);
//            if (rs.next()) {
//                flag = rs.getString(1);
//            }
//            rs = stmt.executeQuery("Select question_Id from Class_Test_Question_Info where testId=" + testId);
//            while (rs.next()) {
//                int q = rs.getInt(1);
//                queIds.add(q);
//            }
//            for (int i = 0; i < queIds.size(); i++) {
//                rs = stmt.executeQuery("Select * from Question_Info where Question_Id=" + queIds.get(i));
//                if (rs.next()) {
//                    QuestionBean questionBean=new QuestionBean();
//                    
//                    questionBean.setQuestionId(rs.getInt(1)) ;
//                    questionBean.setQuestion(rs.getString(2));
//                    questionBean.setOptionA(rs.getString(3));
//                    int view = 0;
//                    questionBean.setOptionB(rs.getString(4)) ;
//                    questionBean.setOptionC (rs.getString(5));
//                    questionBean.setOptionD (rs.getString(6));
//                    questionBean.setAnswer(rs.getString(7));
//                    questionBean.setHint(rs.getString(8));
//                    questionBean.setLevel(rs.getInt(9));
//                    questionBean.setSubjectId(rs.getInt(10));
//                    questionBean.setChapterId(rs.getInt(11));
//                    questionBean.setTopicId(rs.getInt(12));
//                    questionBean.setQuestionImagePath(rs.getString(14));
//                    questionBean.setOptionImagePath(rs.getString(16));
//                    questionBean.setHintImagePath(rs.getString(18));
//                    
//                    boolean result = (rs.getInt(13) == 0) ? false : true;
//                    boolean result1 = (rs.getInt(15) == 0) ? false : true;
//                    boolean result2 = (rs.getInt(17) == 0) ? false : true;
//                    
//                    questionBean.setIsQuestionAsImage(result);
//                    questionBean.setIsOptionAsImage(result1 );
//                    questionBean.setIsHintAsImage(result2);
//                    
//                    questionBean.setAttempt(rs.getInt(19));
//                    questionBean.setType(rs.getInt(20));
//                    questionBean.setYear(rs.getString(21));
//                    
//                    
//                   // QuestionBean q = new QuestionBean(question, A, B, C, D, Answer,optionImagePath,hintImagePath,QuestionImagePath,qyear,question_Id,sub_Id, Chap_Id, Top_Id,attempt,level,type,view,result,result1,result2,Hint);
//                    questionBeanList.add(questionBean);
//                }
//            }
//            unitTestBean.setQuestions(questionBeanList);
//            unitTestBean.setStatus(flag);
//            return unitTestBean;
//        } catch (SQLException ex) {
//            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        return unitTestBean;
//    } 
   
   public int getClassTestTime(int testId) {
        int time = 0;
        conn =  new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select testTime from Class_Test_Info where test_Id=" + testId);
            if (rs.next()) {
                time = rs.getInt(1);
            }
            return time;
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return time;
    }
    public ArrayList<RankBean> calculateRank(int row) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<RankBean> rankBeanList = new ArrayList<RankBean>();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select * from Class_Test_Result_Info where test_Id=" + row + " order by OBTAIN_MARK DESC");
            while (rs.next()) {
                RankBean rankBean = new RankBean();
                rankBean.setUnitTestId(rs.getInt(1));
               rankBean.setRollNo(rs.getInt(2));
                rankBean.setSubject_Id(rs.getInt(3));
                rankBean.setTotal_Questions(rs.getInt(4));
                rankBean.setCorrect_Questions((int) rs.getInt(5));
                rankBean.setIncorrect_Questions((int) rs.getInt(6));
                rankBean.setTestbeanId(rs.getInt(8));
                rankBean.setObtainMark(rs.getDouble(9));
                rankBean.setTotalMark(rs.getDouble(10));
                rankBean.setPHY_CORRECT_QUESTIONS(rs.getDouble(11));
                rankBean.setCHEM_CORRECT_QUESTIONS(rs.getDouble(12));
                rankBean.setMATH_CORRECT_QUESTIONS(rs.getDouble(13));
                rankBean.setPHY_INCORRECT_QUESTIONS(rs.getDouble(14));
                rankBean.setCHEM_INCORRECT_QUESTIONS(rs.getDouble(15));
                rankBean.setMATH_INCORRECT_QUESTIONS(rs.getDouble(16));
                rankBean.setTOTAL_PHY_QUESTIONS(rs.getDouble(17));
                rankBean.setTOTAL_CHEM_QUESTIONS(rs.getDouble(18));
                rankBean.setTOTAL_MATH_QUESTIONS(rs.getDouble(19));
                rankBean.setTOTAL_PHY_MARK(rs.getDouble(20));
                rankBean.setTOTAL_CHEM_MARK(rs.getDouble(21));
                rankBean.setTOTAL_MATH_MARK(rs.getDouble(22));
                Timestamp timeStamp = rs.getTimestamp(7);
                java.sql.Date date = new java.sql.Date(timeStamp.getTime());
                String str = date.toString();
                rankBean.setTimeinstring(str);
                rankBeanList.add(rankBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return rankBeanList;
    }
    
    public boolean setTestToPerform1(int row) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        boolean ReturnValue=false;
        ArrayList<Integer> qId = new ArrayList<Integer>();
        try {
            Statement s1 = conn.createStatement();
            Statement s2 = conn.createStatement();
            Statement s3 = conn.createStatement();
            Statement s4 = conn.createStatement();
            Statement s5 = conn.createStatement();
            Statement s6 = conn.createStatement();
            Statement s7 = conn.createStatement();
            Statement s8 = conn.createStatement();
            Statement s9 = conn.createStatement();
            
            s4.execute("delete from CurrentTestStatusOfStudent");
            s4.execute("delete from Temp_Class_Test_Info");
            s5.execute("delete from Temp_Class_Test_Question_Info");

            rs = s1.executeQuery("Select * from Class_Test_Info where test_Id=" + row);
            
            if (rs.next()) {
                s2.execute("insert into Temp_Class_Test_Info values (" + rs.getInt(1) + "," + rs.getInt(2) + ",'Available','" + rs.getTimestamp(4) + "'," + rs.getInt(5) + ")");
            }
            
            rs = s1.executeQuery("Select * from Class_Test_Question_Info where testId=" + row);
            while (rs.next()) {
                qId.add(rs.getInt(2));
            }
            ArrayList<Integer> aidp = new ArrayList<Integer>();
            ArrayList<Integer> aidc = new ArrayList<Integer>();
            ArrayList<Integer> aidb = new ArrayList<Integer>();
            ArrayList<Integer> aidg = new ArrayList<Integer>();
            for (int i = 0; i < qId.size(); i++) {
                // check subject count for multiple sub
                ResultSet resultSet = s6.executeQuery("select Subject_Id from Question_Info where question_Id=" + qId.get(i));
                if (resultSet.next()) {
                    int temp = resultSet.getInt(1);
                    if (temp == 1) {
                        aidp.add(qId.get(i));
                    } else if (temp == 2) {
                        aidc.add(qId.get(i));
                    } else if (temp == 3) {
                        aidb.add(qId.get(i));
                    } else if (temp == 4) {
                        aidg.add(qId.get(i));
                    }
                }
            }



            qId = new ArrayList<Integer>();
            for (int i = 0; i < aidp.size(); i++) {
                qId.add(aidp.get(i));
            }
            for (int i = 0; i < aidc.size(); i++) {
                qId.add(aidc.get(i));
            }
            for (int i = 0; i < aidb.size(); i++) {
                qId.add(aidb.get(i));
            }
            for (int i = 0; i < aidg.size(); i++) {
                qId.add(aidg.get(i));
            }
            char c[];
            String ss = "ABCD";
            c = ss.toCharArray();
            if (qId.size() > 0) {
                String newAlpha = "A";
                for (int i = 0; i < 4; i++) {
                    newAlpha = Character.toString(c[i]);
                    for (int y = 0; y < aidp.size(); y++) {
                        s3.execute("insert into Temp_Class_Test_Question_Info values(" + row + "," + aidp.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidc.size(); y++) {
                        s7.execute("insert into Temp_Class_Test_Question_Info values(" + row + "," + aidc.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidb.size(); y++) {
                        s8.execute("insert into Temp_Class_Test_Question_Info values(" + row + "," + aidb.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidg.size(); y++) {
                        s9.execute("insert into Temp_Class_Test_Question_Info values(" + row + "," + aidg.get(y) + ",'" + newAlpha + "')");
                    }
                    Collections.shuffle(aidp);
                    Collections.shuffle(aidc);
                    Collections.shuffle(aidb);
                    Collections.shuffle(aidg);
                }
            }
            //Update Practice_Info set status='Finished' where Practice_Id=" + practiceId);
            s1.executeUpdate("Update Test_And_Set set status='Available' where test_Id=" + row);
            return ReturnValue=true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ReturnValue=true;
    }
    
     public String getTestName(int Test_Id) {
        String testName = null;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
                rs = stmt.executeQuery("Select * from Class_Test_Info where test_Id="+Test_Id);
                if (rs.next()) {
                    testName = rs.getString(6);
                    return testName;
                }
         
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testName;
}
     
  public boolean deleteTest(int TestId)  {
        
        boolean returnValue = false;
        int lastId = new NewIdOperation().getNewId1("CLASS_TEST_INFO");
        lastId -= 1;
        try {
            int currentTestId = TestId;
            conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            String query;
            
            query = "DELETE FROM SMSHISTORY WHERE TEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            query = "DELETE FROM TEST_AND_SET WHERE TEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            if(lastId != currentTestId) {
                for(int i=currentTestId; i<lastId; i++) {
                    query = "UPDATE TEST_AND_SET SET TEST_ID = ? WHERE TEST_ID = ?";
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, i); 
                    ps.setInt(2, i + 1);
                    ps.executeUpdate();
                }
            } 
            query = "DELETE FROM TEMP_CLASS_TEST_INFO WHERE TEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            query = "DELETE FROM TEMP_CLASS_TEST_QUESTION_INFO WHERE TESTID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            query = "DELETE FROM CLASS_TEST_QUESTION_INFO WHERE TESTID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            if(lastId != currentTestId) {
                for(int i=currentTestId; i<lastId; i++) {
                    query = "UPDATE CLASS_TEST_QUESTION_INFO SET TESTID = ? WHERE TESTID = ?";
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, i); 
                    ps.setInt(2, i + 1);
                    ps.executeUpdate();
                }
            } 
            
            conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            
            query = "DELETE FROM CLASS_TEST_QUESTION_STATUS_INFO WHERE TEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            if(lastId != currentTestId) {
                for(int i=currentTestId; i<lastId; i++) {
                    query = "UPDATE CLASS_TEST_QUESTION_STATUS_INFO SET TEST_ID = ? WHERE TEST_ID = ?";
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, i); 
                    ps.setInt(2, i + 1);
                    ps.executeUpdate();
                }
            } 
            
            query = "DELETE FROM CLASS_TEST_RESULT_INFO WHERE TEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
            if(lastId != currentTestId) {
                for(int i=currentTestId; i<lastId; i++) {
                    query = "UPDATE CLASS_TEST_RESULT_INFO SET TEST_ID = ? WHERE TEST_ID = ?";
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, i); 
                    ps.setInt(2, i + 1);
                    ps.executeUpdate();
                }
            } 
            
            query = "DELETE FROM CLASS_TEST_INFO WHERE TEST_ID= ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentTestId);
            ps.executeUpdate();
            
           conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            
            if(lastId != currentTestId) {
                for(int i=currentTestId; i<lastId; i++) {
                    query = "UPDATE CLASS_TEST_INFO SET TEST_ID = ? WHERE TEST_ID = ?";
                    ps = conn.prepareStatement(query);
                    ps.setInt(1, i); 
                    ps.setInt(2, i + 1);
                    ps.executeUpdate();
                }
            }    
            returnValue = true;
            
            } catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
           sqlClose();
        }
        return returnValue;
  }
  
   public ArrayList<ClassTestBean> getClassTestInfo1() {
        ArrayList<ClassTestBean> classTestList = new ArrayList<ClassTestBean>();
        ClassTestBean classTestBean=null;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select * from  Class_Test_Info order by test_Id");
            while (rs.next()) {
//                String temp = rs.getInt(1) + "\t" + rs.getBoolean(2) + "\t" + rs.getTimestamp(3);
                classTestBean=new ClassTestBean();
                classTestBean.setTestId(rs.getInt(1));
                classTestBean.setGroupId(rs.getInt(2));
                classTestBean.setStatus(rs.getString(3));
                classTestBean.setTestDate(rs.getTimestamp(4).toString());
                classTestBean.setTestTotalTime(rs.getInt(5));
                classTestBean.setTestName(rs.getString(6));
                classTestList.add(classTestBean);
               
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return classTestList;
    } 
   
    public boolean setTestToPerform2(ClassTestBean  classTestBean) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        boolean ReturnValue=false;
        ArrayList<Integer> qId = new ArrayList<Integer>();
        try {
            Statement s1 = conn.createStatement();
            Statement s2 = conn.createStatement();
            Statement s3 = conn.createStatement();
            Statement s4 = conn.createStatement();
            Statement s5 = conn.createStatement();
            Statement s6 = conn.createStatement();
            Statement s7 = conn.createStatement();
            Statement s8 = conn.createStatement();
            Statement s9 = conn.createStatement();
            
            s4.execute("delete from CurrentTestStatusOfStudent");
            s4.execute("delete from Temp_Class_Test_Info");
            s5.execute("delete from Temp_Class_Test_Question_Info");

            rs = s1.executeQuery("Select * from Class_Test_Info where test_Id=" + classTestBean.getTestId());
            
            if (rs.next()) {
                s2.execute("insert into Temp_Class_Test_Info values (" + rs.getInt(1) + "," + rs.getInt(2) + ",'Available','" + rs.getTimestamp(4) + "'," + classTestBean.getTestTotalTime() + ",'" + classTestBean.getTestName() + "','"+classTestBean.getAcademicYear()+"','"+classTestBean.getClass_Std()+"','"+classTestBean.getDivission()+"','"+classTestBean.getSTART_DATE()+"','"+classTestBean.getEXPIRE_DATE()+"')");
            }
            
            rs = s1.executeQuery("Select * from Class_Test_Question_Info where testId=" + classTestBean.getTestId());
            while (rs.next()) {
                qId.add(rs.getInt(2));
            }
            ArrayList<Integer> aidp = new ArrayList<Integer>();
            ArrayList<Integer> aidc = new ArrayList<Integer>();
            ArrayList<Integer> aidb = new ArrayList<Integer>();
            ArrayList<Integer> aidg = new ArrayList<Integer>();
            for (int i = 0; i < qId.size(); i++) {
                // check subject count for multiple sub
                ResultSet resultSet = s6.executeQuery("select Subject_Id from Question_Info where question_Id=" + qId.get(i));
                if (resultSet.next()) {
                    int temp = resultSet.getInt(1);
                    if (temp == 1) {
                        aidp.add(qId.get(i));
                    } else if (temp == 2) {
                        aidc.add(qId.get(i));
                    } else if (temp == 3) {
                        aidb.add(qId.get(i));
                    } else if (temp == 4) {
                        aidg.add(qId.get(i));
                    }
                }
            }



            qId = new ArrayList<Integer>();
            for (int i = 0; i < aidp.size(); i++) {
                qId.add(aidp.get(i));
            }
            for (int i = 0; i < aidc.size(); i++) {
                qId.add(aidc.get(i));
            }
            for (int i = 0; i < aidb.size(); i++) {
                qId.add(aidb.get(i));
            }
            for (int i = 0; i < aidg.size(); i++) {
                qId.add(aidg.get(i));
            }
            char c[];
            String ss = "ABCD";
            c = ss.toCharArray();
            if (qId.size() > 0) {
                String newAlpha = "A";
                for (int i = 0; i < 4; i++) {
                    newAlpha = Character.toString(c[i]);
                    for (int y = 0; y < aidp.size(); y++) {
                        s3.execute("insert into Temp_Class_Test_Question_Info values(" + classTestBean.getTestId() + "," + aidp.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidc.size(); y++) {
                        s7.execute("insert into Temp_Class_Test_Question_Info values(" + classTestBean.getTestId() + "," + aidc.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidb.size(); y++) {
                        s8.execute("insert into Temp_Class_Test_Question_Info values(" + classTestBean.getTestId() + "," + aidb.get(y) + ",'" + newAlpha + "')");
                    }
                    for (int y = 0; y < aidg.size(); y++) {
                        s9.execute("insert into Temp_Class_Test_Question_Info values(" + classTestBean.getTestId() + "," + aidg.get(y) + ",'" + newAlpha + "')");
                    }
                    Collections.shuffle(aidp);
                    Collections.shuffle(aidc);
                    Collections.shuffle(aidb);
                    Collections.shuffle(aidg);
                }
            }
            //Update Practice_Info set status='Finished' where Practice_Id=" + practiceId);
            s1.executeUpdate("Update Test_And_Set set status='Available' where test_Id=" +classTestBean.getTestId());
            return ReturnValue=true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ReturnValue=true;
    }
  
     public ArrayList<ClassTestBean> getClassTestInfobytestid(int testid) {
        ArrayList<ClassTestBean> classTestList = new ArrayList<ClassTestBean>();
        ClassTestBean classTestBean=null;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select * from  Class_Test_Info where test_Id = " +  testid  +"  order by test_Id");
            while (rs.next()) {
//                String temp = rs.getInt(1) + "\t" + rs.getBoolean(2) + "\t" + rs.getTimestamp(3);
                classTestBean=new ClassTestBean();
                classTestBean.setTestId(rs.getInt(1));
                classTestBean.setGroupId(rs.getInt(2));
                classTestBean.setStatus(rs.getString(3));
                classTestBean.setTestDate(rs.getTimestamp(4).toString());
                classTestBean.setTestTotalTime(rs.getInt(5));
                classTestBean.setTestName(rs.getString(6));
                classTestList.add(classTestBean);
               
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return classTestList;
    } 
    
     
     public boolean resetTestToPerform2(ClassTestBean  classTestBean) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        boolean ReturnValue=false;
        
        ArrayList<Integer> qId = new ArrayList<Integer>();
        try {
            Statement s1 = conn.createStatement();
            Statement s2 = conn.createStatement();
            Statement s3 = conn.createStatement();
//            Statement s4 = conn.createStatement();
//            Statement s5 = conn.createStatement();
            Statement s6 = conn.createStatement();
            Statement s7 = conn.createStatement();
            Statement s8 = conn.createStatement();
            Statement s9 = conn.createStatement();
            
//            s4.execute("delete from CurrentTestStatusOfStudent");
//            s4.execute("delete from Temp_Class_Test_Info");
//            s5.execute("delete from Temp_Class_Test_Question_Info");

                            rs = s1.executeQuery("Select * from Class_Test_Info where test_Id=" + classTestBean.getTestId());

                            if (rs.next()) {
//                                s2.execute("insert into Temp_Class_Test_Info values (" + rs.getInt(1) + "," + rs.getInt(2) + ",'Available','" + rs.getTimestamp(4) + "'," + classTestBean.getTestTotalTime() + ",'" + classTestBean.getTestName() + "','"+classTestBean.getAcademicYear()+"','"+classTestBean.getClass_Std()+"','"+classTestBean.getDivission()+"','"+classTestBean.getSTART_DATE()+"','"+classTestBean.getEXPIRE_DATE()+"')");

                            
                                 String query ="UPDATE TEMP_CLASS_TEST_INFO SET GROUP_ID =? ,STATUS = ?,TEST_DATETIME = ?,TESTTIME = ?,TEST_NAME = ?,ACADEMIC_YEAR = ?,CLASS_STD = ?,DIVISSION = ?,START_DATE = ?,EXPIRE_DATE = ? WHERE TEST_ID=? ";
                                 ps = conn.prepareStatement(query);
                                
                                
                                 ps.setInt(1, rs.getInt(2));
                                 ps.setString(2, "Available");
                                 ps.setTimestamp(3, rs.getTimestamp(4));
                                 ps.setInt(4,classTestBean.getTestTotalTime());
                                 ps.setString(5,classTestBean.getTestName());
                                 ps.setString(6,classTestBean.getAcademicYear());
                                 ps.setString(7,classTestBean.getClass_Std());
                                 ps.setString(8,classTestBean.getDivission());
                                 ps.setString(9,classTestBean.getSTART_DATE());
                                 ps.setString(10,classTestBean.getEXPIRE_DATE());
                                 ps.setInt(11, rs.getInt(1));
                                 ps.executeUpdate();
                            }

                            rs = s1.executeQuery("Select * from Class_Test_Question_Info where testId=" + classTestBean.getTestId());
                            while (rs.next()) {
                                qId.add(rs.getInt(2));
                            }
                            ArrayList<Integer> aidp = new ArrayList<Integer>();
                            ArrayList<Integer> aidc = new ArrayList<Integer>();
                            ArrayList<Integer> aidb = new ArrayList<Integer>();
                            ArrayList<Integer> aidg = new ArrayList<Integer>();
                            for (int i = 0; i < qId.size(); i++) {
                                // check subject count for multiple sub
                                ResultSet resultSet = s6.executeQuery("select Subject_Id from Question_Info where question_Id=" + qId.get(i));
                                if (resultSet.next()) {
                                    int temp = resultSet.getInt(1);
                                    if (temp == 1) {
                                        aidp.add(qId.get(i));
                                    } else if (temp == 2) {
                                        aidc.add(qId.get(i));
                                    } else if (temp == 3) {
                                        aidb.add(qId.get(i));
                                    } else if (temp == 4) {
                                        aidg.add(qId.get(i));
                                    }
                                }
                            }



                            qId = new ArrayList<Integer>();
                            for (int i = 0; i < aidp.size(); i++) {
                                qId.add(aidp.get(i));
                            }
                            for (int i = 0; i < aidc.size(); i++) {
                                qId.add(aidc.get(i));
                            }
                            for (int i = 0; i < aidb.size(); i++) {
                                qId.add(aidb.get(i));
                            }
                            for (int i = 0; i < aidg.size(); i++) {
                                qId.add(aidg.get(i));
                            }
                            char c[];
                            String ss = "ABCD";
                            c = ss.toCharArray();
                            if (qId.size() > 0) {
                                String newAlpha = "A";
                                for (int i = 0; i < 4; i++) {
                                    newAlpha = Character.toString(c[i]);
                                    for (int y = 0; y < aidp.size(); y++) {
//                                        s3.execute("insert into Temp_Class_Test_Question_Info values(" + classTestBean.getTestId() + "," + aidp.get(y) + ",'" + newAlpha + "')");
                                          s3.executeUpdate("UPDATE TEMP_CLASS_TEST_QUESTION_INFO SET TESTID = " + classTestBean.getTestId() + ",QUESTION_ID =" + aidp.get(y) + " ,SETINITIALS = '" + newAlpha + "' WHERE TESTID= "+classTestBean.getTestId());
                                    }
                                    for (int y = 0; y < aidc.size(); y++) {
//                                        s7.execute("insert into Temp_Class_Test_Question_Info values(" + classTestBean.getTestId() + "," + aidc.get(y) + ",'" + newAlpha + "')");
                                          s7.executeUpdate("UPDATE TEMP_CLASS_TEST_QUESTION_INFO SET TESTID = " + classTestBean.getTestId() + ",QUESTION_ID =" + aidc.get(y) + " ,SETINITIALS = '" + newAlpha + "' WHERE TESTID= "+classTestBean.getTestId());
                                    }
                                    for (int y = 0; y < aidb.size(); y++) {
//                                        s8.execute("insert into Temp_Class_Test_Question_Info values(" + classTestBean.getTestId() + "," + aidb.get(y) + ",'" + newAlpha + "')");
                                          s8.executeUpdate("UPDATE TEMP_CLASS_TEST_QUESTION_INFO SET TESTID = " + classTestBean.getTestId() + ",QUESTION_ID =" + aidb.get(y) + " ,SETINITIALS = '" + newAlpha + "' WHERE TESTID= "+classTestBean.getTestId());  
                                    }
                                    for (int y = 0; y < aidg.size(); y++) {
//                                        s9.execute("insert into Temp_Class_Test_Question_Info values(" + classTestBean.getTestId() + "," + aidg.get(y) + ",'" + newAlpha + "')");
                                          s9.executeUpdate("UPDATE TEMP_CLASS_TEST_QUESTION_INFO SET TESTID = " + classTestBean.getTestId() + ",QUESTION_ID =" + aidg.get(y) + " ,SETINITIALS = '" + newAlpha + "' WHERE TESTID= "+classTestBean.getTestId());  
                                    }
                                    Collections.shuffle(aidp);
                                    Collections.shuffle(aidc);
                                    Collections.shuffle(aidb);
                                    Collections.shuffle(aidg);
                                }
                            }
                            //Update Practice_Info set status='Finished' where Practice_Id=" + practiceId);
                            s1.executeUpdate("Update Test_And_Set set status='Available' where test_Id=" +classTestBean.getTestId());
                            return ReturnValue=true;
            
            
          
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    
                }
            }
        }
        return ReturnValue=true;
    }   
     
    public boolean getStatustestid(ClassTestBean  classTestBean) { 
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        boolean ReturnValue=false;
        Statement s1;
        try {
            s1 = conn.createStatement();
            rs = s1.executeQuery("Select * from Temp_Class_Test_Info where test_Id=" + classTestBean.getTestId());
            while (rs.next()) {
                return ReturnValue=true; 
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return ReturnValue=true; 
    }  
     
     public ArrayList<ClassTestBean> getTempClassTestInfo() {
        ArrayList<ClassTestBean> classTestList = new ArrayList<ClassTestBean>();
        ClassTestBean classTestBean=null;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select * from  Temp_Class_Test_Info order by test_Id");
            while (rs.next()) {
//                String temp = rs.getInt(1) + "\t" + rs.getBoolean(2) + "\t" + rs.getTimestamp(3);
                classTestBean=new ClassTestBean();
                classTestBean.setTestId(rs.getInt(1));
                classTestBean.setGroupId(rs.getInt(2));
                classTestBean.setStatus(rs.getString(3));
                classTestBean.setTestDate(rs.getTimestamp(4).toString());
                classTestBean.setTestTotalTime(rs.getInt(5));
                classTestBean.setTestName(rs.getString(6));
                classTestBean.setAcademicYear(rs.getString(7));
                classTestBean.setClass_Std(rs.getString(8));
                classTestBean.setDivission(rs.getString(9));
                classTestBean.setSTART_DATE(rs.getString(10));
                classTestBean.setEXPIRE_DATE(rs.getString(11));
                classTestList.add(classTestBean);
               
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return classTestList;
    } 
     
      public ArrayList<ClassTestBean> getTempClassTestInfo1(int row) {
        ArrayList<ClassTestBean> classTestList = new ArrayList<ClassTestBean>();
        ClassTestBean classTestBean=null;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select * from  Temp_Class_Test_Info where test_Id="+row);
            while (rs.next()) {
//                String temp = rs.getInt(1) + "\t" + rs.getBoolean(2) + "\t" + rs.getTimestamp(3);
                classTestBean=new ClassTestBean();
                classTestBean.setTestId(rs.getInt(1));
                classTestBean.setGroupId(rs.getInt(2));
                classTestBean.setStatus(rs.getString(3));
                classTestBean.setTestDate(rs.getTimestamp(4).toString());
                classTestBean.setTestTotalTime(rs.getInt(5));
                classTestBean.setTestName(rs.getString(6));
                classTestBean.setAcademicYear(rs.getString(7));
                classTestBean.setClass_Std(rs.getString(8));
                classTestBean.setDivission(rs.getString(9));
                classTestBean.setStatus(rs.getString(10));
                classTestBean.setEXPIRE_DATE(rs.getString(11));
                classTestList.add(classTestBean);
               
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return classTestList;
    } 
     
      
    public ArrayList<RankBean> getResultReport(int Rollno) {
        conn =  new DBConnection1().getClientConnection1(Server.getServerIP());
        ArrayList<RankBean> rankBeanList = new ArrayList<RankBean>();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select * from Class_Test_Result_Info where ROLLNO=" + Rollno + " order by TEST_ID ");
            while (rs.next()) {
                RankBean rankBean = new RankBean();
                rankBean.setUnitTestId(rs.getInt(1));
               rankBean.setRollNo(rs.getInt(2));
                rankBean.setSubject_Id(rs.getInt(3));
                rankBean.setTotal_Questions(rs.getInt(4));
                rankBean.setCorrect_Questions((int) rs.getInt(5));
                rankBean.setIncorrect_Questions((int) rs.getInt(6));
                rankBean.setTestbeanId(rs.getInt(8));
                rankBean.setObtainMark(rs.getDouble(9));
                rankBean.setTotalMark(rs.getDouble(10));
                rankBean.setPHY_CORRECT_QUESTIONS(rs.getDouble(11));
                rankBean.setCHEM_CORRECT_QUESTIONS(rs.getDouble(12));
                rankBean.setMATH_CORRECT_QUESTIONS(rs.getDouble(13));
                rankBean.setPHY_INCORRECT_QUESTIONS(rs.getDouble(14));
                rankBean.setCHEM_INCORRECT_QUESTIONS(rs.getDouble(15));
                rankBean.setMATH_INCORRECT_QUESTIONS(rs.getDouble(16));
                rankBean.setTOTAL_PHY_QUESTIONS(rs.getDouble(17));
                rankBean.setTOTAL_CHEM_QUESTIONS(rs.getDouble(18));
                rankBean.setTOTAL_MATH_QUESTIONS(rs.getDouble(19));
                rankBean.setTOTAL_PHY_MARK(rs.getDouble(20));
                rankBean.setTOTAL_CHEM_MARK(rs.getDouble(21));
                rankBean.setTOTAL_MATH_MARK(rs.getDouble(22));
                Timestamp timeStamp = rs.getTimestamp(7);
                java.sql.Date date = new java.sql.Date(timeStamp.getTime());
                String str = date.toString();
                rankBean.setTimeinstring(str);
                rankBeanList.add(rankBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return rankBeanList;
    }  
    
   public Boolean getClassTestStatus(int testid) {
        Boolean Status=false;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select * from  CLASS_TEST_RESULT_INFO where TEST_ID = " +  testid  );
            while (rs.next()) {
                 return Status=true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ClassSaveTestOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return Status;
    } 
        private void sqlClose() {
              try {
                  if(rs != null)
                      rs.close();
                  if(ps != null)
                      ps.close();
                  if(conn != null)
                      conn.close();
              } catch (SQLException ex) {
                  ex.printStackTrace();
              }
          }
}
