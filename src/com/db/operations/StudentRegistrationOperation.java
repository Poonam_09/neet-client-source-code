/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.StudentBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.DBConnection1;
import server.Server;

/**
 *
 * @author Aniket
 */
public class StudentRegistrationOperation {
    
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null,ps1=null,ps2=null;
    private Statement stmt=null,stmt1=null;
    public ArrayList< StudentBean> getAllInfoStudent(int rollno) {
        
        
        ArrayList<StudentBean> studentBeanList = null;
        StudentBean studentBean=null;
        String RollNO=Integer.toString(rollno);
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            stmt = conn.createStatement();
            //rollno,name,pwd,std,div,year
             rs = stmt.executeQuery("select * from Student_Info where FULLNAME='" + RollNO+ "'");
            if (rs.next()) {
                 studentBeanList = new ArrayList<StudentBean>();
                 studentBean=new StudentBean();
                 studentBean.setRollno(rs.getInt(1));
                 studentBean.setFullname(rs.getString(2));
                 studentBean.setPass(rs.getString(3));
                 studentBean.setStandard(rs.getString(4));
                 studentBean.setDivision(rs.getString(5));
                 studentBean.setAcademicYear(rs.getString(6));
                 studentBean.setStudentName(rs.getString(7));
                 studentBean.setMobileNo(rs.getString(8));
                 studentBeanList.add(studentBean);
                 return studentBeanList;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(StudentRegistrationOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentBeanList;
    }
}
