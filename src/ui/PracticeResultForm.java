package ui;

import java.awt.Color;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import JEE.practice.*;

import JEE.unitTest.UnitTestBean;
import JEE.unitTest.UnitTestResultBean;
import com.bean.ClassTestBean;
import com.bean.QuestionBean;
import com.bean.RankBean;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author Avadhut
 */
public class PracticeResultForm extends javax.swing.JFrame {

    ArrayList<QuestionBean> alQuestions;
    int subjectId, chapterId, i = 0, corr = 0, incorr = 0,phycorr=0,chemcorr=0,Biocorr=0,phyincorr=0,chemincorr=0,Bioincorr=0;
    BufferedImage image;
    JLabel label = new JLabel();
    JButton button = new JButton();
    int rollNo;
    Connection con1;
    JEE.unitTest.DBConnection con=null;
    ArrayList<RankBean> rb = new ArrayList<RankBean>();
    String userAnswer;
    UnitTestBean unitTestBean;
    ClassTestBean classTestBean=new ClassTestBean();
    /** Creates new form TestResultForm */
    public PracticeResultForm() {
        initComponents();
        
    }
    public PracticeResultForm(int rollNo) {
        initComponents();
        this.rollNo = rollNo;
    }

    void btnStrtActionPerformed(java.awt.event.ActionEvent evt, int que) {
        if (que < alQuestions.size()) {
            QuestionBean questionBean = alQuestions.get(que);
            new QuestionAnswerForm(questionBean, que + 1).setVisible(true);
        }
    }

    public void tablesetting() {
        this.getContentPane().setBackground(Color.getColor("C4DFFE"));
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screen);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        pkg.Renderer renderer = new pkg.Renderer();
//        renderer.setHorizontalAlignment( CENTER );
        tblPractice.setModel(new MyModel());
        tblPractice.setDefaultRenderer(JLabel.class, renderer);// for the rendering of cell 
        tblPractice.setRowHeight(36);

        tblPractice.getColumnModel().getColumn(1).setMaxWidth(200);
        tblPractice.getColumnModel().getColumn(1).setMinWidth(200);
        tblPractice.getColumnModel().getColumn(1).setWidth(200);
        tblPractice.getColumnModel().getColumn(1).setPreferredWidth(200);
        tblPractice.getColumnModel().getColumn(2).setMaxWidth(200);
        tblPractice.getColumnModel().getColumn(2).setMinWidth(200);
        tblPractice.getColumnModel().getColumn(2).setWidth(200);
        tblPractice.getColumnModel().getColumn(2).setPreferredWidth(200);

        tblPractice.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

        int w = this.getWidth();
        int parentWidth = jPanel2.getWidth();
        int childWidth = panePhysics.getWidth();
        int diff = parentWidth - childWidth;
        int x = diff / 2;
        panePhysics.setLocation(x, 0);
        panePhysics.setSize(childWidth + 50, jPanel2.getHeight() + 120);
        panePhysics.setPreferredSize(new Dimension(childWidth + 50, jPanel2.getHeight() + 120));
        //resultTabbedPane.setSize(diff, diff);
        System.out.println(panePhysics.getPreferredSize());
        this.revalidate();
        this.repaint();
    }

    public PracticeResultForm(UnitTestBean unitTestBean, int rollNo) {
        initComponents();
        this.rollNo = rollNo;
        String StudentName= new DBConnection().getStudentName(rollNo);
        lblDisplayName.setText(StudentName);
        lblDiplayRollNo.setText(Integer.toString(rollNo));
        tablesetting();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        button.setText("View Details");
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = -1;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        if (unitTestBean != null) {
            JEE.unitTest.DBConnection con = new JEE.unitTest.DBConnection();
            alQuestions = unitTestBean.getQuestions();
            int totalQuestions = 0;
            double correctQuestions = 0.0,marksPerQuestion = 0.0, ngtvmarks = 0.0;
            if (alQuestions.size() > 0) {
                DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();

                Iterator<QuestionBean> it = alQuestions.iterator();
                //TableCellRenderer buttonRenderer = new JTableButtonRenderer();
                //tblPhysics.getColumn("button1").setCellRenderer(buttonRenderer);		
                while (it.hasNext()) {
                    
                    QuestionBean questionBean = it.next();
                    marksPerQuestion = con.getPerQuestionMarks(questionBean.getSubjectId());
                    ngtvmarks = con.getPerwrongQuestionMarks(questionBean.getSubjectId());
                    String userAnswer = con.getResult(unitTestBean.getUnitTestId(), rollNo,questionBean.getQuestionId());
                    if (userAnswer != null) {
                        int y = i + 1;
                        if (userAnswer.equals("unAttempted")) {
                            JLabel label = new JLabel();
                            if (i == 0) {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true);
                            } else if (i == alQuestions.size() - 1) {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true);
                            } else {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true);
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true);
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y, label, label1});
                        } else {
                            if (userAnswer.equals(questionBean.getAnswer())) {
                                JLabel label = new JLabel();
                                if (i == 0) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/" + questionBean.getUserAnswer() + ".png")));
                                } else if (i == alQuestions.size() - 1) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/" + questionBean.getUserAnswer() + ".png")));
                                } else {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/" + questionBean.getUserAnswer() + ".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true);
                                model.addRow(new Object[]{y, label, label1});
                                correctQuestions = correctQuestions + marksPerQuestion;
                                corr++;
                            } else {
                                JLabel label = new JLabel();
                                if (i == 0) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/" + questionBean.getUserAnswer() + ".png")));
                                } else if (i == alQuestions.size() - 1) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/" + questionBean.getUserAnswer() + ".png")));
                                } else {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/" + questionBean.getUserAnswer() + ".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true);
                                model.addRow(new Object[]{y, label, label1});
                                if (ngtvmarks <= 0) {
                                    correctQuestions = correctQuestions + ngtvmarks;
                                } else {
                                    correctQuestions = correctQuestions - ngtvmarks;
                                }
                                incorr++;
                            }
                        }
                        totalQuestions++;
                    }
                    i++;
                }
//                model.addRow(new Object[]{"","",""});
//                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
//                model.addRow(new Object[]{"Total Marks :",(correctQuestions)+"/"+(totalQuestions*marksPerQuestion),""});
            } else {
            }
            UnitTestResultBean unitTestResultBean = new UnitTestResultBean(unitTestBean.getUnitTestId(), totalQuestions, corr, incorr);
//            PracticeResultBean practiceResultBean = new PracticeResultBean(practiceBean.getPracticeId(), totalQuestions, correctQuestions);
            con.addResult(unitTestResultBean, rollNo);
        }
        System.exit(0);
        
    }

    public PracticeResultForm(UnitTestBean unitTestBean, int rollNo, boolean b,int Group_Id,ClassTestBean classTestBean) {
        initComponents();
        this.classTestBean=classTestBean;
        lblNote.setVisible(false);
        this.unitTestBean=unitTestBean;
        this.rollNo = rollNo;
        System.out.println("rollNo*********************"+rollNo);
        String StudentName= new DBConnection().getStudentName(rollNo);
        lblDisplayName.setText(StudentName);
        lblDiplayRollNo.setText(Integer.toString(rollNo));
        tablesetting();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        button.setText("View Details");
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = -1;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        if (unitTestBean != null) {
            System.out.println("Welcome to Score Card");
            JEE.unitTest.DBConnection con = new JEE.unitTest.DBConnection();
            alQuestions = unitTestBean.getQuestions();
            int totalQuestions = 0,totalPhyQues=0,totalChemQues=0,totalBioQues=0;
            double TotalMarks = 0,totalPhyMarks=0,totalChemMark=0,totalBioMark=0;
            double correctQuestions = 0.0,PhycorrectQuestions=0.0,ChemcorrectQuestions=0.0,BiocorrectQuestions=0.0;
            double marksPerQuestion = 0.0, ngtvmarks,PhymarksPerQuestion=0.0,ChemmarksPerQuestion=0.0,BiomarksPerQuestion=0.0;
            
            PhymarksPerQuestion = con.getPerQuestionMarks(1);
            ChemmarksPerQuestion = con.getPerQuestionMarks(2);
            BiomarksPerQuestion = con.getPerQuestionMarks(4);
            
            double Phyngtvmarks = con.getPerwrongQuestionMarks(1);
            double Chemngtvmarks = con.getPerwrongQuestionMarks(2);
            double Biongtvmarks = con.getPerwrongQuestionMarks(4);
            
            if (alQuestions.size() > 0) {
                DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();

                Iterator<QuestionBean> it = alQuestions.iterator();
                //TableCellRenderer buttonRenderer = new JTableButtonRenderer();
                //tblPhysics.getColumn("button1").setCellRenderer(buttonRenderer);
                int j = 1;
                while (it.hasNext()) {
                    
                    QuestionBean questionBean = it.next();
                    marksPerQuestion = con.getPerQuestionMarks(questionBean.getSubjectId());
                    ngtvmarks = con.getPerwrongQuestionMarks(questionBean.getSubjectId());
                    String userAnswer = con.getResult1(unitTestBean.getUnitTestId(),rollNo, questionBean.getQuestionId() );
                    System.out.println("Swapna " + userAnswer);
                    System.out.println("questionBean.getQuestionId() " + questionBean.getQuestionId());

                      totalPhyQues=con.getPhyCount(1,unitTestBean.getUnitTestId());
                      totalChemQues=con.getChemCount(2,unitTestBean.getUnitTestId());
                      totalBioQues=con.getBioCount(4,unitTestBean.getUnitTestId());
                      
                      totalPhyMarks=(totalPhyQues*PhymarksPerQuestion);
                      totalChemMark=(totalChemQues*ChemmarksPerQuestion);
                      totalBioMark=(totalBioQues*BiomarksPerQuestion);
                      
                      TotalMarks= totalPhyMarks + totalChemMark + totalBioMark;
                    j++;
                    if (userAnswer != null) {
                        int y = i + 1;
                        if (questionBean.getUserAnswer().equals("UnAttempted")) {
                            JLabel label = new JLabel();
                            if (i == 0) {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true);
                            } else if (i == alQuestions.size() - 1) {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true);
                            } else {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true);
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true);
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y, label, label1});
                        } else {
                            if (questionBean.getUserAnswer().equals(questionBean.getAnswer())) {  //userAnswer.equals(questionBean.getAnswer()
                                JLabel label = new JLabel();
                                if (i == 0) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/" + questionBean.getUserAnswer() + ".png")));
                                } else if (i == alQuestions.size() - 1) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/" + questionBean.getUserAnswer() + ".png")));
                                } else {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/" + questionBean.getUserAnswer() + ".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true);
                                model.addRow(new Object[]{y, label, label1});
                                correctQuestions = correctQuestions + marksPerQuestion;
                                corr++;
                                        if(questionBean.getSubjectId()==1 )
                                        { 
                                            PhycorrectQuestions = PhycorrectQuestions + PhymarksPerQuestion;
                                                phycorr++;
                                        }else if(questionBean.getSubjectId()==2 )
                                        {
                                            ChemcorrectQuestions = ChemcorrectQuestions + ChemmarksPerQuestion;
                                                chemcorr++;
                                        }else if(questionBean.getSubjectId()==4 )
                                        {
                                            BiocorrectQuestions = BiocorrectQuestions + BiomarksPerQuestion;
                                                Biocorr++;
                                        }
                                       
                            } else {
                                JLabel label = new JLabel();
                                if (i == 0) {
                                    System.out.println(questionBean.getUserAnswer());
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/" + questionBean.getUserAnswer() + ".png")));
                                } else if (i == alQuestions.size() - 1) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/" + questionBean.getUserAnswer() + ".png")));
                                } else {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/" + questionBean.getUserAnswer() + ".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true);
                                model.addRow(new Object[]{y, label, label1});
                                if (ngtvmarks <= 0) {
                                    correctQuestions = correctQuestions + ngtvmarks;
                                } else {
                                    correctQuestions = correctQuestions - ngtvmarks;
                                }
                                incorr++;
                                
                                if(questionBean.getSubjectId()==1 )
                                {
                                    if (ngtvmarks <= 0) {
                                       PhycorrectQuestions = PhycorrectQuestions + Phyngtvmarks;
                                    } else {
                                       PhycorrectQuestions = PhycorrectQuestions - Phyngtvmarks;
                                    }
                                     phyincorr++;
                                }else if(questionBean.getSubjectId()==2 )
                                {
                                    if (ngtvmarks <= 0) {
                                        ChemcorrectQuestions = ChemcorrectQuestions + Chemngtvmarks;
                                    } else {
                                        ChemcorrectQuestions = ChemcorrectQuestions - Chemngtvmarks;
                                    }
                                      chemincorr++;
                                }else if(questionBean.getSubjectId()==4 )
                                {
                                    if (ngtvmarks <= 0) {
                                        BiocorrectQuestions = BiocorrectQuestions + Biongtvmarks;
                                    } else {
                                        BiocorrectQuestions = BiocorrectQuestions - Biongtvmarks;
                                    }
                                      Bioincorr++;
                                }
                            }
                        }
                        totalQuestions++;
                    }
                    i++;
                    
                }
                model.addRow(new Object[]{"", "", ""});
                model.addRow(new Object[]{"", "Physics Correct:" + phycorr, " Physics Incorrect:" + phyincorr});
                model.addRow(new Object[]{"", "Chemistry Correct:" + chemcorr, "Chemistry Incorrect:" + chemincorr});
                model.addRow(new Object[]{"", "Biology Correct:" + Biocorr, "Biology Incorrect:" + Bioincorr});
                model.addRow(new Object[]{"", "Total Correct:" + corr, "Total Incorrect:" + incorr});
                model.addRow(new Object[]{"","Total Physics Marks :", (PhycorrectQuestions) + "/" + totalPhyMarks + ""});
                model.addRow(new Object[]{"","Total Chemistry Marks :", (ChemcorrectQuestions) + "/" + totalChemMark + ""});
                model.addRow(new Object[]{"","Total Biology Marks :", (BiocorrectQuestions) + "/" + totalBioMark + ""});
                model.addRow(new Object[]{"","Total Marks :", (correctQuestions) + "/" + TotalMarks + ""});
            } else {
            }
            int mo = 0;
            try {
                mo = con.InsertTestDetails(unitTestBean.getQuestions(),rollNo);
            } catch (IOException ex) {
                Logger.getLogger(PracticeResultForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(mo>0){
               correctQuestions=new RestrctDoubleUpto2().round(correctQuestions, 2);
               PhycorrectQuestions=new RestrctDoubleUpto2().round(PhycorrectQuestions, 2);
               ChemcorrectQuestions=new RestrctDoubleUpto2().round(ChemcorrectQuestions, 2);
               BiocorrectQuestions=new RestrctDoubleUpto2().round(BiocorrectQuestions, 2);
               con.addResult1(unitTestBean.getUnitTestId(), rollNo, unitTestBean.getSubjectId(), unitTestBean.getQuestions().size(), corr, incorr,correctQuestions,TotalMarks,phycorr,chemcorr,Biocorr,phyincorr,chemincorr,Bioincorr,totalPhyQues,totalChemQues,totalBioQues,PhycorrectQuestions,ChemcorrectQuestions,BiocorrectQuestions,classTestBean );
            }
        }
//        new NewTestForm(rollNo).setVisible(true);
       
        this.setVisible(true);
    }
    
    public PracticeResultForm(UnitTestBean unitTestBean, int rollNo, boolean b,ClassTestBean classTestBean) {
        initComponents();
        this.classTestBean=classTestBean;
        lblNote.setVisible(false);
        this.unitTestBean=unitTestBean;
        this.rollNo = rollNo;
        System.out.println("rollNo*********************"+rollNo);
        String StudentName= new DBConnection().getStudentName(rollNo);
        lblDisplayName.setText(StudentName);
        lblDiplayRollNo.setText(Integer.toString(rollNo));
        tablesetting();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        button.setText("View Details");
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = -1;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        if (unitTestBean != null) {
            System.out.println("Welcome to Score Card");
            JEE.unitTest.DBConnection con = new JEE.unitTest.DBConnection();
            alQuestions = unitTestBean.getQuestions();
            int totalQuestions = 0,totalPhyQues=0,totalChemQues=0,totalBioQues=0;
            double TotalMarks = 0,totalPhyMarks=0,totalChemMark=0,totalBioMark=0;
            double correctQuestions = 0.0,PhycorrectQuestions=0.0,ChemcorrectQuestions=0.0,BiocorrectQuestions=0.0;
            double marksPerQuestion = 0.0, ngtvmarks,PhymarksPerQuestion=0.0,ChemmarksPerQuestion=0.0,BiomarksPerQuestion=0.0;
            
            PhymarksPerQuestion = con.getPerQuestionMarks(1);
            ChemmarksPerQuestion = con.getPerQuestionMarks(2);
            BiomarksPerQuestion = con.getPerQuestionMarks(4);
            
            double Phyngtvmarks = con.getPerwrongQuestionMarks(1);
            double Chemngtvmarks = con.getPerwrongQuestionMarks(2);
            double Biongtvmarks = con.getPerwrongQuestionMarks(4);
            
            if (alQuestions.size() > 0) {
                DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();

                Iterator<QuestionBean> it = alQuestions.iterator();
                //TableCellRenderer buttonRenderer = new JTableButtonRenderer();
                //tblPhysics.getColumn("button1").setCellRenderer(buttonRenderer);
                int j = 1;
                while (it.hasNext()) {
                    
                    QuestionBean questionBean = it.next();
                    marksPerQuestion = con.getPerQuestionMarks(questionBean.getSubjectId());
                    ngtvmarks = con.getPerwrongQuestionMarks(questionBean.getSubjectId());
                    String userAnswer = con.getResult1(unitTestBean.getUnitTestId(),rollNo, questionBean.getQuestionId() );
                    System.out.println("Swapna " + userAnswer);
                    System.out.println("questionBean.getQuestionId() " + questionBean.getQuestionId());
//                    TotalMarks=(totalQuestions * marksPerQuestion);
                      totalPhyQues=con.getPhyCount(1,unitTestBean.getUnitTestId());
                      totalChemQues=con.getChemCount(2,unitTestBean.getUnitTestId());
                      totalBioQues=con.getBioCount(4,unitTestBean.getUnitTestId());
                      
                      totalPhyMarks=(totalPhyQues*PhymarksPerQuestion);
                      totalChemMark=(totalChemQues*ChemmarksPerQuestion);
                      totalBioMark=(totalBioQues*BiomarksPerQuestion);
                      
                      TotalMarks= totalPhyMarks + totalChemMark + totalBioMark;
                    j++;
                    if (userAnswer != null) {
                        int y = i + 1;
                        if (questionBean.getUserAnswer().equals("UnAttempted")) {
                            JLabel label = new JLabel();
                            if (i == 0) {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true);
                            } else if (i == alQuestions.size() - 1) {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true);
                            } else {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true);
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true);
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y, label, label1});
                        } else {
                            if (questionBean.getUserAnswer().equals(questionBean.getAnswer())) { //userAnswer.equals(questionBean.getAnswer())
                                JLabel label = new JLabel();
                                if (i == 0) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/" + questionBean.getUserAnswer() + ".png")));
                                } else if (i == alQuestions.size() - 1) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/" + questionBean.getUserAnswer() + ".png")));
                                } else {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/" + questionBean.getUserAnswer() + ".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true);
                                model.addRow(new Object[]{y, label, label1});
                                correctQuestions = correctQuestions + marksPerQuestion;
                                corr++;
                                        if(questionBean.getSubjectId()==1 )
                                        { 
                                            PhycorrectQuestions = PhycorrectQuestions + PhymarksPerQuestion;
                                                phycorr++;
                                        }else if(questionBean.getSubjectId()==2 )
                                        {
                                            ChemcorrectQuestions = ChemcorrectQuestions + ChemmarksPerQuestion;
                                                chemcorr++;
                                        }else if(questionBean.getSubjectId()==4 )
                                        {
                                            BiocorrectQuestions = BiocorrectQuestions + BiomarksPerQuestion;
                                                Biocorr++;
                                        }
                                       
                            } else {
                                JLabel label = new JLabel();
                                if (i == 0) {
                                    System.out.println(questionBean.getUserAnswer());
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/" + questionBean.getUserAnswer() + ".png")));
                                } else if (i == alQuestions.size() - 1) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/" + questionBean.getUserAnswer() + ".png")));
                                } else {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/" + questionBean.getUserAnswer() + ".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true);
                                model.addRow(new Object[]{y, label, label1});
                                if (ngtvmarks <= 0) {
                                    correctQuestions = correctQuestions + ngtvmarks;
                                } else {
                                    correctQuestions = correctQuestions - ngtvmarks;
                                }
                                incorr++;
                                
                                if(questionBean.getSubjectId()==1 )
                                {
                                    if (ngtvmarks <= 0) {
                                       PhycorrectQuestions = PhycorrectQuestions + Phyngtvmarks;
                                    } else {
                                       PhycorrectQuestions = PhycorrectQuestions - Phyngtvmarks;
                                    }
                                     phyincorr++;
                                }else if(questionBean.getSubjectId()==2 )
                                {
                                    if (ngtvmarks <= 0) {
                                        ChemcorrectQuestions = ChemcorrectQuestions + Chemngtvmarks;
                                    } else {
                                        ChemcorrectQuestions = ChemcorrectQuestions - Chemngtvmarks;
                                    }
                                      chemincorr++;
                                }else if(questionBean.getSubjectId()==4 )
                                {
                                    if (ngtvmarks <= 0) {
                                        BiocorrectQuestions = BiocorrectQuestions + Biongtvmarks;
                                    } else {
                                        BiocorrectQuestions = BiocorrectQuestions - Biongtvmarks;
                                    }
                                      Bioincorr++;
                                }
                            }
                        }
                        totalQuestions++;
                    }
                    i++;
                    
                }
                model.addRow(new Object[]{"", "", ""});            
                model.addRow(new Object[]{"", "Correct:" + corr, " Incorrect:" + incorr});               
                model.addRow(new Object[]{"","Total Marks :", (correctQuestions) + "/" + TotalMarks + ""});
            } else {
            }
            int mo = 0;
            try {
                mo = con.InsertTestDetails(unitTestBean.getQuestions(),rollNo);
            } catch (IOException ex) {
                Logger.getLogger(PracticeResultForm.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(mo>0){
               correctQuestions=new RestrctDoubleUpto2().round(correctQuestions, 2);
               PhycorrectQuestions=new RestrctDoubleUpto2().round(PhycorrectQuestions, 2);
               ChemcorrectQuestions=new RestrctDoubleUpto2().round(ChemcorrectQuestions, 2);
               BiocorrectQuestions=new RestrctDoubleUpto2().round(BiocorrectQuestions, 2);
               con.addResult1(unitTestBean.getUnitTestId(), rollNo, unitTestBean.getSubjectId(), unitTestBean.getQuestions().size(), corr, incorr,correctQuestions,TotalMarks,phycorr,chemcorr,Biocorr,phyincorr,chemincorr,Bioincorr,totalPhyQues,totalChemQues,totalBioQues,PhycorrectQuestions,ChemcorrectQuestions,BiocorrectQuestions,classTestBean );
            }
        }
//        new NewTestForm(rollNo).setVisible(true);
       
        this.setVisible(true);
    }

    public void loadImage() {
        try {
            File file = new File("/ui/images/opt.gif");
            image = ImageIO.read(file);
            ImageIcon icon;
            float width = image.getWidth();
            float height = image.getHeight();
            Image thumb = image.getScaledInstance((int) width, (int) height, Image.SCALE_AREA_AVERAGING);
            icon = new ImageIcon(thumb);
            lblNote.setIcon(icon);
            lblNote.setText("");
            this.setSize((int) width + 50, (int) height + 50);
            this.setLocationRelativeTo(null);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public PracticeResultForm(PracticeBean practiceBean, int rollNo) {
        initComponents();
        this.rollNo = rollNo;
        tablesetting();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        if (practiceBean != null) {
            DBConnection con = new DBConnection();
            int i = 0;
            alQuestions = practiceBean.getQuestions();
            int totalQuestions = 0;
            double correctQuestions = 0.0,marksPerQuestion = 0.0, ngtvmarks;
            if (alQuestions.size() > 0) {
                DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();
                Iterator<QuestionBean> it = alQuestions.iterator();
                while (it.hasNext()) {
                    
                    QuestionBean questionBean = it.next();
                    marksPerQuestion = con.getPerQuestionMarks(questionBean.getSubjectId());
                    ngtvmarks = con.getPerwrongQuestionMarks(questionBean.getSubjectId());
                    String userAnswer = con.getResult(practiceBean.getPracticeId(), rollNo,questionBean.getQuestionId());
                    if (userAnswer != null) {
                        int y = i + 1;
                        if (userAnswer.equals("unAttempted")) {
                            JLabel label = new JLabel();
                            if (i == 0) {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true);
                            } else if (i == alQuestions.size() - 1) {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true);
                            } else {
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true);
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true);
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y, label, label1});
                        } else {
                            if (userAnswer.equals(questionBean.getAnswer())) {
                                JLabel label = new JLabel();
                                if (i == 0) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/" + questionBean.getUserAnswer() + ".png")));
                                } else if (i == alQuestions.size() - 1) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/" + questionBean.getUserAnswer() + ".png")));
                                } else {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/" + questionBean.getUserAnswer() + ".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true);
                                model.addRow(new Object[]{y, label, label1});
                                correctQuestions = correctQuestions + marksPerQuestion;
                                corr++;
                            } else {
                                JLabel label = new JLabel();
                                if (i == 0) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/" + questionBean.getUserAnswer() + ".png")));
                                } else if (i == alQuestions.size() - 1) {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/" + questionBean.getUserAnswer() + ".png")));
                                } else {
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/" + questionBean.getUserAnswer() + ".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true);
                                model.addRow(new Object[]{y, label, label1});
                                if (ngtvmarks <= 0) {
                                    correctQuestions = correctQuestions + ngtvmarks;
                                } else {
                                    correctQuestions = correctQuestions - ngtvmarks;
                                }
                                incorr++;
                            }
                        }
                        totalQuestions++;
                    }
                    i++;
                }
                model.addRow(new Object[]{"", "", ""});
                model.addRow(new Object[]{"", "Correct:" + corr, "Incorrect:" + incorr});
                model.addRow(new Object[]{"Total Marks :", (correctQuestions) + "/" + (totalQuestions * marksPerQuestion), ""});
            } else {
            }
            PracticeResultBean practiceResultBean = new PracticeResultBean(practiceBean.getPracticeId(), totalQuestions, correctQuestions);
//            PracticeResultBean practiceResultBean = new PracticeResultBean(practiceBean.getPracticeId(), totalQuestions, correctQuestions);
            con.addResult(practiceResultBean, rollNo);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        lblNote = new javax.swing.JLabel();
        btnHome = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        panePhysics = new javax.swing.JScrollPane();
        tblPractice = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        }
        ;
        lblName = new javax.swing.JLabel();
        lblDisplayName = new javax.swing.JLabel();
        lblRollNo = new javax.swing.JLabel();
        lblDiplayRollNo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CruncherSoft's NEET Client Application");
        setBackground(new java.awt.Color(196, 223, 254));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(196, 223, 254));
        jPanel1.setName("jPanel1"); // NOI18N

        jPanel3.setBackground(new java.awt.Color(196, 223, 254));
        jPanel3.setName("jPanel3"); // NOI18N

        lblNote.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        lblNote.setText("Double Click On Question No. To Get Result In Detail...!");
        lblNote.setDebugGraphicsOptions(javax.swing.DebugGraphics.FLASH_OPTION);
        lblNote.setName("lblNote"); // NOI18N
        lblNote.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblNoteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblNoteMouseExited(evt);
            }
        });

        btnHome.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        btnHome.setText("Home");
        btnHome.setName("btnHome"); // NOI18N
        btnHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHomeMouseExited(evt);
            }
        });
        btnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHomeActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setFont(new java.awt.Font("Verdana", 1, 36)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("SCORECARD");
        lblTitle.setName("lblTitle"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 779, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblNote, javax.swing.GroupLayout.PREFERRED_SIZE, 393, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnHome)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNote, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnHome))
                .addGap(22, 22, 22))
        );

        jPanel2.setBackground(new java.awt.Color(196, 223, 254));
        jPanel2.setName("jPanel2"); // NOI18N

        panePhysics.setFont(new java.awt.Font("Century", 1, 11)); // NOI18N
        panePhysics.setName("panePhysics"); // NOI18N

        ((DefaultTableCellRenderer)tblPractice.getTableHeader().getDefaultRenderer())
        .setHorizontalAlignment(JLabel.CENTER);
        tblPractice.setFont(new java.awt.Font("Verdana", 1, 15)); // NOI18N
        tblPractice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Question No.", "User Answer", "Result"
            }
        ));
        tblPractice.setToolTipText("Double Click To View Detail Result.");
        tblPractice.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblPractice.setName("tblPractice"); // NOI18N
        tblPractice.setSelectionForeground(new java.awt.Color(204, 0, 0));
        tblPractice.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblPractice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPracticeMouseClicked(evt);
            }
        });
        panePhysics.setViewportView(tblPractice);

        jPanel2.add(panePhysics);

        lblName.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        lblName.setText("Name :-");
        lblName.setName("lblName"); // NOI18N

        lblDisplayName.setName("lblDisplayName"); // NOI18N

        lblRollNo.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        lblRollNo.setText("Roll No. :-");
        lblRollNo.setName("lblRollNo"); // NOI18N

        lblDiplayRollNo.setName("lblDiplayRollNo"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblDisplayName, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblRollNo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDiplayRollNo, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 799, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDisplayName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblRollNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDiplayRollNo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void lblNoteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblNoteMouseEntered
    lblNote.setBackground(Color.red);
    lblNote.setForeground(Color.BLUE);
}//GEN-LAST:event_lblNoteMouseEntered

private void lblNoteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblNoteMouseExited
    lblNote.setBackground(Color.BLUE);
    lblNote.setForeground(Color.red);
}//GEN-LAST:event_lblNoteMouseExited

private void btnHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMouseEntered
    btnHome.setForeground(Color.red);
}//GEN-LAST:event_btnHomeMouseEntered

private void btnHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMouseExited
    btnHome.setForeground(Color.black);
}//GEN-LAST:event_btnHomeMouseExited

private void btnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHomeActionPerformed
   
    this.dispose();
}//GEN-LAST:event_btnHomeActionPerformed

private void tblPracticeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPracticeMouseClicked
    if (evt.getClickCount() == 2) {
        int row = tblPractice.rowAtPoint(evt.getPoint());
        if (row < alQuestions.size()) {
            QuestionBean questionBean = alQuestions.get(row);
            new QuestionAnswerForm(questionBean, row + 1).setVisible(true);
        }
    }
//       if (evt.getClickCount() == 2) {
//       con = new JEE.unitTest.DBConnection();
//        rb = con.calculateRank(unitTestBean.getUnitTestId());
//        System.out.println("View  : " + rollNo);
//            for (int ii = 0; ii < rb.size(); ii++) {
//                if (rollNo == rb.get(ii).getRollNo()) {
//                    RankBean rankBean = new RankBean();
//                    rankBean = rb.get(ii);
//                    int testBeanId = rankBean.getTestbeanId();
//                    System.out.println("testBeanId="+testBeanId);
//                    int rollNo = rankBean.getRollNo();
//                    ArrayList<QuestionBean> qb = new ArrayList<QuestionBean>();
//                    qb =con.getTestDetails(rollNo, testBeanId);
//                    UnitTestBean ub = new UnitTestBean();
//                    ub.setQuestions(qb);
//                    ub.setUnitTestId(testBeanId);
//                    ub.setTotalTime(rollNo);
////                    for (int i = 0; i < qb.size(); i++) {
////                        System.out.println(qb.get(i).getQuestion_Id() + ":" + qb.get(i).getUserAnswer() + "-->" + qb.get(i).getAnswer());
////                    }
//                    ViewStudentTestDetails vstd = new ViewStudentTestDetails(ub, false, false, testBeanId, this,rankBean);
//                    vstd.setVisible(true);
//                    this.dispose();
//                }
//            }
   
//        }
}//GEN-LAST:event_tblPracticeMouseClicked
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
//                new PracticeResultForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnHome;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblDiplayRollNo;
    private javax.swing.JLabel lblDisplayName;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNote;
    private javax.swing.JLabel lblRollNo;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JScrollPane panePhysics;
    private javax.swing.JTable tblPractice;
    // End of variables declaration//GEN-END:variables
}