package ui;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ViewStudentTestDetails.java
 *
 * Created on May 24, 2013, 10:52:32 AM
 */



import JEE.unitTest.UnitTestBean;
import com.bean.QuestionBean;
import com.bean.RankBean;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.*;


/**
 *
 * @author 007
 */
public class ViewStudentTestReportDetails extends javax.swing.JFrame {

    QP_1 currentPanel = null;
    ArrayList<QuestionBean> alQuestions;
    boolean flagSubject = false, isNewTest = true, chkflag = false;
    int currentIndex, testid, rollNo;   
    UnitTestBean unitTestBean;
    JButton jButtonsArray[];
    int animationTime = 5, type = 0;
    NumberFormat format;
    ButtonGroup btnGroupAnimation;
    RankBean rankBean;
    JEE.unitTest.DBConnection con=null;
    TestReport testReport =new TestReport();
    String PatternName=null;
    /**
     * Creates new form TestResultForm
     */
    public ViewStudentTestReportDetails() {
        btnGroupAnimation = new ButtonGroup();
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        this.setState(JFrame.MAXIMIZED_BOTH);
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        cl.show(jPanelsSliding1, "card4");
        currentPanel = qP1;
        
        this.getContentPane().setBackground(Color.white);
        jLabel1.setVisible(false);
        rdoEnable.setVisible(false);
        rdoDisable.setVisible(false);
    }

    public void setPanel(boolean flag) {
        QuestionBean questionBean = alQuestions.get(currentIndex);
//        System.out.println(questionBean.getSubjectId());
//        String SubjectName =con.getSubjectName(questionBean.getSubjectId());
//        lblSubjectName.setText( SubjectName);
        if (questionBean.getHint().equals("\\mbox{null}") || questionBean.getHint().equals("\\mbox{ }") || questionBean.getHint().equals("\\mbox{  }") || questionBean.getHint().equals("\\mbox{   }") || questionBean.getHint().equals("\\mbox{\\mbox{}}") || questionBean.getHint().equals("\\mbox{") || questionBean.getHint().equals("\\mbox{    }") || questionBean.getHint().equals("") || questionBean.getHint() == null || questionBean.getHint().equals("\\mbox{}")) {
            btnHint.setVisible(false);
        } else {
            btnHint.setVisible(true);
        }
        if (questionBean.getUserAnswer().equals("UnAttempted")) {
            if (questionBean.getView() == 1) {
                jButtonsArray[currentIndex].setBackground(Color.yellow);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
        currentPanel = (currentPanel == qP1) ? qP2 : qP1;
        currentPanel.lockSelection();
        //set Question        
        currentPanel.setQuestionOnPanel(alQuestions.get(currentIndex), (currentIndex + 1),PatternName);
        txtQuestionNumber.setText((currentIndex + 1) + "");
        //slide panel    
        jPanelsSliding1.nextSlidPanel(animationTime, currentPanel, flag);
        System.out.println("Time : " + animationTime);
        validate();
        revalidate();
        repaint();
        jPanelsSliding1.refresh();
    }

    public void setQuestion(String actionCommand) {
//        setred();
        currentIndex = Integer.parseInt(actionCommand.split(" ")[1]);
//        System.out.print(unitTestBean.getStatus());
        setPanel(false);
        jScrollPane1.validate();
            jScrollPane1.repaint();
            jPanelsSliding1.validate();
            jPanelsSliding1.repaint();
    }

    public void setButtonOnPanel(JPanel queButtonPanels) {
        int subid = unitTestBean.getSubjectId();
        jButtonsArray = new JButton[unitTestBean.getQuestions().size()];
        for (int x = 0; x < unitTestBean.getQuestions().size(); x++) {
            jButtonsArray[x] = new javax.swing.JButton();
            jButtonsArray[x].setActionCommand(subid + " " + x);
//                if(currentPanel){
//            jButtonsArray[x].setBackground(Color.white);
            QuestionBean q = alQuestions.get(x);
            if (q.getUserAnswer().equals("UnAttempted")) {
                if (q.getView() == 1) {
                    jButtonsArray[currentIndex].setBackground(Color.yellow);
                } 
            } else {
                jButtonsArray[x].setBackground(Color.green);
            }
            jButtonsArray[x].setToolTipText("Not Answered");
            jButtonsArray[x].setSize(50, 50);
            jButtonsArray[x].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setQuestion(e.getActionCommand());
                }
            });
            int y = x + 1;
            jButtonsArray[x].setText("" + y);
        }
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(1, 3, 1, 3);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < unitTestBean.getQuestions().size(); x++) {
            if (x % 15 == 0) {
                cons.gridx++;
                cons.gridy = 1;
            }
            layout.setConstraints(jButtonsArray[x], cons);
            queButtonPanels.setLayout(layout);
            queButtonPanels.add(jButtonsArray[x], cons);
            cons.gridy++;
        }
        pnlAllQue.validate();
        pnlAllQue.revalidate();
        pnlAllQue.repaint();
    }

    
    
    public ViewStudentTestReportDetails(UnitTestBean unitTestBean, boolean newTest, boolean just, int testid, TestReport testReport,RankBean rankBean,String PatternName) {
        initComponents();
        type = 1;
        this.rankBean=rankBean;
//        this.rg = rg;
        this.PatternName=PatternName;
        this.testReport=testReport;
        this.testid = testid;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        btnGroupAnimation = new ButtonGroup();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        
        this.unitTestBean = unitTestBean;
        currentPanel = qP1;
        this.alQuestions = unitTestBean.getQuestions();
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        //cl.show(jPanelsSliding1, "card4");
        this.rollNo = unitTestBean.getTotalTime();
        System.out.println("rollNo View"+rollNo);
        
//       String fullName =con.getNameOfStudent(rollNo);
        
        lblMsg.setText( " User Id:" + rollNo);
        rdoEnable.setActionCommand("Enable");
        jCheckBox1.setVisible(false);
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);
        txtQuestionNumber.setColumns(3);
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        setButtonOnPanel(pnlAllQue);
        setPanel(false);
        jLabel1.setVisible(false);
        rdoEnable.setVisible(false);
        rdoDisable.setVisible(false);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        rdoEnable = new javax.swing.JRadioButton();
        rdoDisable = new javax.swing.JRadioButton();
        btnHint = new javax.swing.JButton();
        jCheckBox1 = new javax.swing.JCheckBox();
        btnHint1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnLast = new javax.swing.JButton();
        btnFirst = new javax.swing.JButton();
        txtQuestionNumber = new javax.swing.JTextField();
        btnNext = new javax.swing.JButton();
        btnPrevious = new javax.swing.JButton();
        lblMsg = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanelsSliding1 = new ui.JPanelsSliding();
        qP1 = new ui.QP_1();
        qP2 = new ui.QP_1();
        jScrollPane2 = new javax.swing.JScrollPane();
        pnlAllQue = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Scholars Katta's NEET");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(29, 9, 44));
        jPanel4.setName("jPanel4"); // NOI18N

        jLabel1.setBackground(new java.awt.Color(29, 9, 44));
        jLabel1.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Animation :");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.setName("jLabel1"); // NOI18N
        jPanel4.add(jLabel1);

        rdoEnable.setBackground(new java.awt.Color(29, 9, 44));
        rdoEnable.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        rdoEnable.setForeground(new java.awt.Color(255, 255, 255));
        rdoEnable.setSelected(true);
        rdoEnable.setText("Enable");
        rdoEnable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoEnable.setName("rdoEnable"); // NOI18N
        rdoEnable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoEnableItemStateChanged(evt);
            }
        });
        jPanel4.add(rdoEnable);

        rdoDisable.setBackground(new java.awt.Color(29, 9, 44));
        rdoDisable.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        rdoDisable.setForeground(new java.awt.Color(255, 255, 255));
        rdoDisable.setText("Disable");
        rdoDisable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoDisable.setName("rdoDisable"); // NOI18N
        rdoDisable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoDisableItemStateChanged(evt);
            }
        });
        jPanel4.add(rdoDisable);

        btnHint.setBackground(new java.awt.Color(255, 255, 255));
        btnHint.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnHint.setForeground(new java.awt.Color(29, 9, 44));
        btnHint.setText("Hint");
        btnHint.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnHint.setName("btnHint"); // NOI18N
        btnHint.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnHintMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHintMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHintMouseExited(evt);
            }
        });
        btnHint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHintActionPerformed(evt);
            }
        });
        jPanel4.add(btnHint);

        jCheckBox1.setBackground(new java.awt.Color(29, 9, 44));
        jCheckBox1.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        jCheckBox1.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox1.setText("Set Test for Students");
        jCheckBox1.setName("jCheckBox1"); // NOI18N
        jCheckBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox1ItemStateChanged(evt);
            }
        });
        jPanel4.add(jCheckBox1);

        btnHint1.setBackground(new java.awt.Color(255, 255, 255));
        btnHint1.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnHint1.setForeground(new java.awt.Color(29, 9, 44));
        btnHint1.setText("Back");
        btnHint1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnHint1.setName("btnHint1"); // NOI18N
        btnHint1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnHint1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHint1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHint1MouseExited(evt);
            }
        });
        btnHint1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHint1ActionPerformed(evt);
            }
        });
        jPanel4.add(btnHint1);

        jPanel2.setBackground(new java.awt.Color(29, 9, 44));
        jPanel2.setName("jPanel2"); // NOI18N

        jPanel1.setBackground(new java.awt.Color(29, 9, 44));
        jPanel1.setName("jPanel1"); // NOI18N

        btnLast.setBackground(new java.awt.Color(255, 255, 255));
        btnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last.png"))); // NOI18N
        btnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLast.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnLast.setName("btnLast"); // NOI18N
        btnLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLastMouseExited(evt);
            }
        });
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });

        btnFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first.png"))); // NOI18N
        btnFirst.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFirst.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnFirst.setName("btnFirst"); // NOI18N
        btnFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnFirstMouseExited(evt);
            }
        });
        btnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFirstActionPerformed(evt);
            }
        });

        txtQuestionNumber.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        txtQuestionNumber.setForeground(new java.awt.Color(29, 9, 44));
        txtQuestionNumber.setText("0000");
        txtQuestionNumber.setName("txtQuestionNumber"); // NOI18N
        txtQuestionNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtQuestionNumberMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtQuestionNumberMouseExited(evt);
            }
        });
        txtQuestionNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQuestionNumberKeyReleased(evt);
            }
        });

        btnNext.setBackground(new java.awt.Color(255, 255, 255));
        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/next.png"))); // NOI18N
        btnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNext.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnNext.setName("btnNext"); // NOI18N
        btnNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNextMouseExited(evt);
            }
        });
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        btnPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnPrevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/prev.png"))); // NOI18N
        btnPrevious.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPrevious.setIconTextGap(0);
        btnPrevious.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnPrevious.setName("btnPrevious"); // NOI18N
        btnPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPreviousMouseExited(evt);
            }
        });
        btnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousActionPerformed(evt);
            }
        });

        lblMsg.setBackground(new java.awt.Color(29, 9, 44));
        lblMsg.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblMsg.setForeground(new java.awt.Color(255, 255, 255));
        lblMsg.setText("Welcome To Unit Test Wizard.");
        lblMsg.setName("lblMsg"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFirst)
                .addGap(5, 5, 5)
                .addComponent(btnPrevious)
                .addGap(5, 5, 5)
                .addComponent(txtQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(btnNext)
                .addGap(5, 5, 5)
                .addComponent(btnLast)
                .addContainerGap(279, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnPrevious, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGap(3, 3, 3)
                            .addComponent(txtQuestionNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(btnNext, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLast, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnFirst, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel2.add(jPanel1);

        jScrollPane1.setBackground(new java.awt.Color(29, 9, 44));
        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jPanelsSliding1.setBackground(new java.awt.Color(255, 255, 255));
        jPanelsSliding1.setName("jPanelsSliding1"); // NOI18N
        jPanelsSliding1.setLayout(new java.awt.CardLayout());

        qP1.setName("qP1"); // NOI18N
        jPanelsSliding1.add(qP1, "card2");

        qP2.setName("qP2"); // NOI18N
        jPanelsSliding1.add(qP2, "card3");

        jScrollPane1.setViewportView(jPanelsSliding1);

        jScrollPane2.setBackground(new java.awt.Color(29, 9, 44));
        jScrollPane2.setName("jScrollPane2"); // NOI18N

        pnlAllQue.setBackground(new java.awt.Color(255, 255, 255));
        pnlAllQue.setName("pnlAllQue"); // NOI18N

        javax.swing.GroupLayout pnlAllQueLayout = new javax.swing.GroupLayout(pnlAllQue);
        pnlAllQue.setLayout(pnlAllQueLayout);
        pnlAllQueLayout.setHorizontalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 133, Short.MAX_VALUE)
        );
        pnlAllQueLayout.setVerticalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 376, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(pnlAllQue);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 776, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void rdoEnableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoEnableItemStateChanged
    if (rdoEnable.isSelected()) {
        animationTime = 5;
    } else {
        animationTime = 1;
    }
}//GEN-LAST:event_rdoEnableItemStateChanged

private void rdoDisableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoDisableItemStateChanged
    if (rdoEnable.isSelected()) {
        animationTime = 5;
    } else {
        animationTime = 1;
    }
}//GEN-LAST:event_rdoDisableItemStateChanged

private void btnHintMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHintMouseClicked
    // TODO add your handling code here:
}//GEN-LAST:event_btnHintMouseClicked

private void btnHintMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHintMouseEntered
    //lblMsg.setText("Can View Hint Regarding Test.");
}//GEN-LAST:event_btnHintMouseEntered

private void btnHintMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHintMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnHintMouseExited

private void btnHintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHintActionPerformed
    if (currentIndex < alQuestions.size()) {
        QuestionBean questionBean = alQuestions.get(currentIndex);

        new HintForm1(questionBean, currentIndex + 1).setVisible(true);
    }
}//GEN-LAST:event_btnHintActionPerformed

private void btnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFirstActionPerformed
    if (currentIndex != 0) {
//        setred();
        currentIndex = 0;
        setPanel(true);
    } else {
        JOptionPane.showMessageDialog(null, "This is First Question.");
    }
}//GEN-LAST:event_btnFirstActionPerformed

private void btnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousActionPerformed
    if (currentIndex != 0) {
//        setred();
        currentIndex--;
        setPanel(true);
    } else {
        JOptionPane.showMessageDialog(null, "This is First Question.");
    }
}//GEN-LAST:event_btnPreviousActionPerformed

private void txtQuestionNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuestionNumberKeyReleased
    if (evt.getKeyCode() == 10) {
        int i = Integer.parseInt(txtQuestionNumber.getText());
        int size = alQuestions.size();
        if ((i < 1) || (i > size)) {
            JOptionPane.showMessageDialog(rootPane, "Out Of Range Index");
        } else {
//                setred();
            currentIndex = i - 1;
            setPanel(false);
        }
    }
}//GEN-LAST:event_txtQuestionNumberKeyReleased
    private void btnNextActionPerformed(java.awt.event.ActionEvent evt, boolean b) {
        int last = alQuestions.size() - 1;
        if (currentIndex == last) {
//        setred();
            currentIndex = 0;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }
private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
    int last = alQuestions.size() - 1;
    if (currentIndex < last) {
//        setred();
        currentIndex++;
        setPanel(false);
    } else {
        JOptionPane.showMessageDialog(null, "This is Last Question.");
    }
}//GEN-LAST:event_btnNextActionPerformed

private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
    int last = alQuestions.size() - 1;
    if (currentIndex < last) {
//        setred();
        currentIndex = last;
        setPanel(false);
    } else {
        JOptionPane.showMessageDialog(null, "This is Last Question.");
    }
}//GEN-LAST:event_btnLastActionPerformed

private void btnFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFirstMouseEntered
    //lblMsg.setText("Switch And View To First Question.");
}//GEN-LAST:event_btnFirstMouseEntered

private void btnFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFirstMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnFirstMouseExited

private void btnPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPreviousMouseEntered
    //lblMsg.setText("Switch And View To Previous Question.");
}//GEN-LAST:event_btnPreviousMouseEntered

private void txtQuestionNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtQuestionNumberMouseEntered
    //lblMsg.setText("Enter Question No. Press Enter To View.");
}//GEN-LAST:event_txtQuestionNumberMouseEntered

private void btnPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPreviousMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnPreviousMouseExited

private void btnNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMouseEntered
    //lblMsg.setText("Switch And View To Next Question.");
}//GEN-LAST:event_btnNextMouseEntered

private void btnNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnNextMouseExited

private void txtQuestionNumberMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtQuestionNumberMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_txtQuestionNumberMouseExited

private void btnLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseEntered
    //lblMsg.setText("Switch And View To Last Question.");
}//GEN-LAST:event_btnLastMouseEntered

private void btnLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnLastMouseExited

    private void jCheckBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox1ItemStateChanged
        int i = 0;
        if (jCheckBox1.isSelected()) {
            i =con.changeStatusOfTest("Available", 1, testid);
        } else {
            i = con.changeStatusOfTest("Not Available", 2, testid);
        }
        if (i == 0) {
            JOptionPane.showMessageDialog(null, "Unable to change status.");
        } else if (i == 1) {
            JOptionPane.showMessageDialog(null, "Status change to Available.");
        } else if (i == 2) {
            JOptionPane.showMessageDialog(null, "Status change to Not Available.");
        }
    }//GEN-LAST:event_jCheckBox1ItemStateChanged

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        testReport.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void btnHint1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHint1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnHint1MouseClicked

    private void btnHint1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHint1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnHint1MouseEntered

    private void btnHint1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHint1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnHint1MouseExited

    private void btnHint1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHint1ActionPerformed
        testReport.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnHint1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(ViewStudentTestDetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(ViewStudentTestDetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(ViewStudentTestDetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(ViewStudentTestDetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new ViewStudentTestDetails().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFirst;
    private javax.swing.JButton btnHint;
    private javax.swing.JButton btnHint1;
    private javax.swing.JButton btnLast;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPrevious;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private ui.JPanelsSliding jPanelsSliding1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblMsg;
    private javax.swing.JPanel pnlAllQue;
    private ui.QP_1 qP1;
    private ui.QP_1 qP2;
    private javax.swing.JRadioButton rdoDisable;
    private javax.swing.JRadioButton rdoEnable;
    private javax.swing.JTextField txtQuestionNumber;
    // End of variables declaration//GEN-END:variables
}