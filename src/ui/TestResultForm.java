package ui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import JEE.test.DBConnection;
import JEE.test.SaveAllTests;
import JEE.test.TestResultBean;
import JEE.test.TestBean;
import com.bean.QuestionBean;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

/**
 *
 * @author Avadhut
 */
public class TestResultForm extends javax.swing.JFrame {
    ArrayList<QuestionBean> alPhysics,alChemistry,alMaths,alBiology;
    TestBean testsBean;
    int corrmaths=0,corrphy=0,corrchem=0;
    int tmaths=0,tphy=0,tchem=0;
    int rollNo;
    /** Creates new form TestResultForm */
    public TestResultForm(int rollNo) {
        initComponents();
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.rollNo=rollNo;
        tablesetting();
    }
    
    public void tablesetting()
    {
        Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screen);
//        this.setLocation(screen.width/4, 0);//RelativeTo(null);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        pkg.Renderer renderer = new pkg.Renderer();
//        renderer.setHorizontalAlignment( CENTER );
        tblPhysics.setModel(new MyModel());
        tblChemistry.setModel(new MyModel());
        tblBiology.setModel(new MyModel());
        tblPhysics.setDefaultRenderer(JLabel.class,  renderer);// for the rendering of cell 
        tblChemistry.setDefaultRenderer(JLabel.class,  renderer);// for the rendering of cell 
        tblBiology.setDefaultRenderer(JLabel.class,  renderer);// for the rendering of cell 
        tblBiology.setRowHeight(36);tblPhysics.setRowHeight(36);tblChemistry.setRowHeight(36);
        
        tblPhysics.getColumnModel().getColumn(1).setMaxWidth(144);
        tblPhysics.getColumnModel().getColumn(1).setMinWidth(144);
        tblPhysics.getColumnModel().getColumn(1).setWidth(144);
        tblPhysics.getColumnModel().getColumn(1).setPreferredWidth(144);
        tblPhysics.getColumnModel().getColumn(2).setMaxWidth(140);
        tblPhysics.getColumnModel().getColumn(2).setMinWidth(140);
        tblPhysics.getColumnModel().getColumn(2).setWidth(140);
        tblPhysics.getColumnModel().getColumn(2).setPreferredWidth(140);
        
        tblChemistry.getColumnModel().getColumn(1).setMaxWidth(144);
        tblChemistry.getColumnModel().getColumn(1).setMinWidth(144);
        tblChemistry.getColumnModel().getColumn(1).setWidth(144);
        tblChemistry.getColumnModel().getColumn(1).setPreferredWidth(144);
        tblChemistry.getColumnModel().getColumn(2).setMaxWidth(140);
        tblChemistry.getColumnModel().getColumn(2).setMinWidth(140);
        tblChemistry.getColumnModel().getColumn(2).setWidth(140);
        tblChemistry.getColumnModel().getColumn(2).setPreferredWidth(140);
        
        tblBiology.getColumnModel().getColumn(1).setMaxWidth(144);
        tblBiology.getColumnModel().getColumn(1).setMinWidth(144);
        tblBiology.getColumnModel().getColumn(1).setWidth(144);
        tblBiology.getColumnModel().getColumn(1).setPreferredWidth(144);
        tblBiology.getColumnModel().getColumn(2).setMaxWidth(140);
        tblBiology.getColumnModel().getColumn(2).setMinWidth(140);
        tblBiology.getColumnModel().getColumn(2).setWidth(140);
        tblBiology.getColumnModel().getColumn(2).setPreferredWidth(140);
        
        tblPhysics.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
        tblChemistry.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
        tblBiology.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
        
        int w = this.getWidth();
        int parentWidth = jPanel2.getWidth();
        int childWidth = resultTabbedPane.getWidth();
        int diff = parentWidth - childWidth;
        int x = diff/2;
        resultTabbedPane.setLocation(x, 0);
        resultTabbedPane.setSize(childWidth+50, jPanel2.getHeight()+120);
        resultTabbedPane.setPreferredSize(new Dimension(childWidth+50, jPanel2.getHeight()+120));
        //resultTabbedPane.setSize(diff, diff);
        System.out.println(resultTabbedPane.getPreferredSize());
        this.revalidate();
        this.repaint();
        Point p = resultTabbedPane.getLocation();
        int j;
    }
    
     public TestResultForm(TestBean testBean,int rollNo) {
        initComponents();
        this.rollNo=rollNo;
        testsBean=testBean; 
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        tablesetting();
        
        if(testBean!=null)
        {
            DBConnection con=new DBConnection();
            int i = 0;
            alPhysics =  testBean.getAlPhysics();
            int totalPhysics=0,totalChemistry=0,totalMaths=0,marksPerQuestion=0,negmarks=0;
            int correctPhysics=0,correctChemistry=0,correctMaths=0;
            
            if(alPhysics.size()>0)
            {                
                int corr=0,incorr=0;
                DefaultTableModel model = (DefaultTableModel) tblPhysics.getModel();
                Iterator<QuestionBean> it=alPhysics.iterator();
                //TableCellRenderer buttonRenderer = new JTableButtonRenderer();
		//tblPhysics.getColumn("button1").setCellRenderer(buttonRenderer);		
                while(it.hasNext())
                {
                    marksPerQuestion=con.getPerQuestionMarks(1);
                    negmarks=con.getPerwrongQuestionMarks(1);
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==0){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==0){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctPhysics=correctPhysics+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==0){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctPhysics=correctPhysics+negmarks;
                                }
                                else{
                                    correctPhysics=correctPhysics-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalPhysics++;
                    }
                    i++;
                }
                corrphy=correctPhysics;
                tphy=totalPhysics*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(correctPhysics)+"/"+(tphy),""});
            }
            else
            {
                resultTabbedPane.remove(panePhysics);
            }
            
            alChemistry =  testBean.getAlChemistry();
            if(alChemistry.size()>0)
            {    
                int corr=0,incorr=0;
                marksPerQuestion = 0;
                marksPerQuestion=con.getPerQuestionMarks(2);
                negmarks=con.getPerwrongQuestionMarks(2);
                DefaultTableModel model = (DefaultTableModel) tblChemistry.getModel();
                Iterator<QuestionBean> it=alChemistry.iterator();                
                while(it.hasNext())
                {
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==alPhysics.size()){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()+alChemistry.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctChemistry=correctChemistry+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctChemistry=correctChemistry+negmarks;
                                }
                                else{
                                    correctChemistry=correctChemistry-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalChemistry ++;
                    }
                    i++;
                }
                corrchem=correctChemistry;
                tchem=totalChemistry*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(correctChemistry)+"/"+(tchem),""});
            }
            else
            {
                resultTabbedPane.remove(paneChemistry);
            }
            alBiology =  testBean.getAlMaths();
            if(alBiology.size()>0)
            {        
                int corr=0,incorr=0;
                marksPerQuestion=con.getPerQuestionMarks(3);
                negmarks=con.getPerwrongQuestionMarks(3);
                DefaultTableModel model = (DefaultTableModel) tblBiology.getModel();
                Iterator<QuestionBean> it=alBiology.iterator();           
                while(it.hasNext())
                {
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==alPhysics.size()+alChemistry.size()){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()+alChemistry.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctMaths=correctMaths+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()+alChemistry.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctMaths=correctMaths+negmarks;
                                }
                                else{
                                    correctMaths=correctMaths-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalMaths++;
                    }
                    i++;
                }
                corrmaths=correctMaths;
                tmaths=totalMaths*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(corrmaths)+"/"+(tmaths),""});
                model.addRow(new Object[]{"Total Marks :",(corrchem+corrmaths+corrphy)+"/"+(tphy+tchem+tmaths),""});
            }
            else
            {
                resultTabbedPane.remove(paneBiology);
            }
            
            TestResultBean resultBean=new TestResultBean(testBean.getTestId(), totalPhysics, totalChemistry,  totalMaths, correctPhysics, correctChemistry,  correctMaths);            
            con.addResult(resultBean,rollNo);
            
        }
    }

     public TestResultForm(TestBean testBean,boolean bool,int rollNo) {
        initComponents();
        this.rollNo=rollNo;
        testsBean=testBean; 
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        tablesetting();
        int negmarks;
        
        if(testBean!=null)
        {
            DBConnection con=new DBConnection();
            int i = 0;
            alPhysics =  testBean.getAlPhysics();
            int totalPhysics=0,totalChemistry=0,totalMaths=0,marksPerQuestion=0;
            int correctPhysics=0,correctChemistry=0,correctMaths=0;
            
            if(alPhysics.size()>0)
            {                
                int corr=0,incorr=0;
                DefaultTableModel model = (DefaultTableModel) tblPhysics.getModel();
                Iterator<QuestionBean> it=alPhysics.iterator();
                //TableCellRenderer buttonRenderer = new JTableButtonRenderer();
		//tblPhysics.getColumn("button1").setCellRenderer(buttonRenderer);		
                while(it.hasNext())
                {
                    marksPerQuestion=con.getPerQuestionMarks(1);
                    negmarks=con.getPerwrongQuestionMarks(1);
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==0){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==0){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctPhysics=correctPhysics+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==0){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctPhysics=correctPhysics+negmarks;
                                }
                                else{
                                    correctPhysics=correctPhysics-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalPhysics++;
                    }
                    i++;
                }
                corrphy=correctPhysics;
                tphy=totalPhysics*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(correctPhysics)+"/"+(tphy),""});
            }
            else
            {
                resultTabbedPane.remove(panePhysics);
            }
            
            alChemistry =  testBean.getAlChemistry();
            if(alChemistry.size()>0)
            {    
                int corr=0,incorr=0;
                marksPerQuestion = 0;
                marksPerQuestion=con.getPerQuestionMarks(2);
                negmarks=con.getPerwrongQuestionMarks(2);
                DefaultTableModel model = (DefaultTableModel) tblChemistry.getModel();
                Iterator<QuestionBean> it=alChemistry.iterator();                
                while(it.hasNext())
                {
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==alPhysics.size()){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()+alChemistry.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctChemistry=correctChemistry+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctChemistry=correctChemistry+negmarks;
                                }
                                else{
                                    correctChemistry=correctChemistry-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalChemistry ++;
                    }
                    i++;
                }
                corrchem=correctChemistry;
                tchem=totalChemistry*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(correctChemistry)+"/"+(tchem),""});
            }
            else
            {
                resultTabbedPane.remove(paneChemistry);
            }
            alBiology =  testBean.getAlMaths();
            if(alBiology.size()>0)
            {        
                int corr=0,incorr=0;
                marksPerQuestion=con.getPerQuestionMarks(3);
                negmarks=con.getPerwrongQuestionMarks(3);
                DefaultTableModel model = (DefaultTableModel) tblBiology.getModel();
                Iterator<QuestionBean> it=alBiology.iterator();           
                while(it.hasNext())
                {
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==alPhysics.size()+alChemistry.size()){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()+alChemistry.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctMaths=correctMaths+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()+alChemistry.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctMaths=correctMaths+negmarks;
                                }
                                else{
                                    correctMaths=correctMaths-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalMaths++;
                    }
                    i++;
                }
                corrmaths=correctMaths;
                tmaths=totalMaths*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(corrmaths)+"/"+(tmaths),""});
                model.addRow(new Object[]{"Total Marks :",(corrchem+corrmaths+corrphy)+"/"+(tphy+tchem+tmaths),""});
            }
            else
            {
                resultTabbedPane.remove(paneBiology);
            }
            TestResultBean resultBean=new TestResultBean(testBean.getTestId(), totalPhysics, totalChemistry, totalMaths, correctPhysics, correctChemistry, correctMaths);            
        }
    }
     
     public TestResultForm(TestBean testBean,boolean bool,boolean b,int rollNo) {
        initComponents();
        this.rollNo=rollNo;
        testsBean=testBean; 
        btnSave.setVisible(false);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        tablesetting();
        int negmarks;
        
        if(testBean!=null)
        {
            DBConnection con=new DBConnection();
            int i = 0;
            alPhysics =  testBean.getAlPhysics();
            int totalPhysics=0,totalChemistry=0,totalMaths=0,marksPerQuestion=0;
            int correctPhysics=0,correctChemistry=0,correctMaths=0;
            
            if(alPhysics.size()>0)
            {                
                int corr=0,incorr=0;
                DefaultTableModel model = (DefaultTableModel) tblPhysics.getModel();
                Iterator<QuestionBean> it=alPhysics.iterator();
                //TableCellRenderer buttonRenderer = new JTableButtonRenderer();
		//tblPhysics.getColumn("button1").setCellRenderer(buttonRenderer);		
                while(it.hasNext())
                {
                    marksPerQuestion=con.getPerQuestionMarks(1);
                    negmarks=con.getPerwrongQuestionMarks(1);
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==0){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==0){
                                    
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctPhysics=correctPhysics+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==0){
                                    System.out.println(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png"));
                                    System.out.println((questionBean.getUserAnswer()));
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctPhysics=correctPhysics+negmarks;
                                }
                                else{
                                    correctPhysics=correctPhysics-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalPhysics++;
                    }
                    i++;
                }
                corrphy=correctPhysics;
                tphy=totalPhysics*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(correctPhysics)+"/"+(tphy),""});
            }
            else
            {
                resultTabbedPane.remove(panePhysics);
            }
            
            alChemistry =  testBean.getAlChemistry();
            if(alChemistry.size()>0)
            {    
                int corr=0,incorr=0;
                marksPerQuestion = 0;
                marksPerQuestion=con.getPerQuestionMarks(2);
                negmarks=con.getPerwrongQuestionMarks(2);
                DefaultTableModel model = (DefaultTableModel) tblChemistry.getModel();
                Iterator<QuestionBean> it=alChemistry.iterator();                
                while(it.hasNext())
                {
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==alPhysics.size()){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()+alChemistry.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctChemistry=correctChemistry+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctChemistry=correctChemistry+negmarks;
                                }
                                else{
                                    correctChemistry=correctChemistry-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalChemistry ++;
                    }
                    i++;
                }
                corrchem=correctChemistry;
                tchem=totalChemistry*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(correctChemistry)+"/"+(tchem),""});
            }
            else
            {
                resultTabbedPane.remove(paneChemistry);
            }
            alBiology =  testBean.getAlMaths();
            if(alBiology.size()>0)
            {        
                int corr=0,incorr=0;
                marksPerQuestion=con.getPerQuestionMarks(3);
                negmarks=con.getPerwrongQuestionMarks(3);
                DefaultTableModel model = (DefaultTableModel) tblBiology.getModel();
                Iterator<QuestionBean> it=alBiology.iterator();           
                while(it.hasNext())
                {
                    QuestionBean questionBean=it.next();
                    String userAnswer = con.getResult(testBean.getTestId(),questionBean.getQuestionId());
                    if(userAnswer!=null)
                    {
                        int y=i+1;
                        if(userAnswer.equals("unAttempted"))
                        {
                            JLabel label = new JLabel();
                            if(i==alPhysics.size()+alChemistry.size()){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/0.png")));
                                label.setOpaque(true); 
                            }
                            else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/0.png")));
                                label.setOpaque(true); 
                            }
                            else{
                                label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/0.png")));
                                label.setOpaque(true); 
                            }
                            JLabel label1 = new JLabel();
                            label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/notatt.png")));
                            label1.setOpaque(true); 
//                            model.addRow(new Object[]{"Row 4 Col 2",label, "Row 4 Col3"});
                            model.addRow(new Object[]{y,label,label1});
                        }
                        else
                        {
                            if(userAnswer.equals(questionBean.getAnswer()))
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()+alChemistry.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/corr.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                correctMaths=correctMaths+marksPerQuestion;
                                corr++;
                            }
                            else
                            {
                                JLabel label = new JLabel();
                                if(i==alPhysics.size()+alChemistry.size()){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first/"+questionBean.getUserAnswer()+".png")));
                                }
                                else if(i==alPhysics.size()+alChemistry.size()+alBiology.size()-1){
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last/"+questionBean.getUserAnswer()+".png")));
                                }
                                else{
                                    label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/mid/"+questionBean.getUserAnswer()+".png")));
                                }
                                label.setOpaque(true);
                                JLabel label1 = new JLabel();
                                label1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wro.png")));
                                label1.setOpaque(true); 
                                model.addRow(new Object[]{y,label,label1});
                                if(negmarks<=0){
                                    correctMaths=correctMaths+negmarks;
                                }
                                else{
                                    correctMaths=correctMaths-negmarks;
                                }
                                incorr++;
                            }
                        }
                        totalMaths++;
                    }
                    i++;
                }
                corrmaths=correctMaths;
                tmaths=totalMaths*marksPerQuestion;
                model.addRow(new Object[]{"","",""});
                model.addRow(new Object[]{"","Correct:"+corr,"Incorrect:"+incorr});
                model.addRow(new Object[]{"Marks :",(corrmaths)+"/"+(tmaths),""});
                model.addRow(new Object[]{"Total Marks :",(corrchem+corrmaths+corrphy)+"/"+(tphy+tchem+tmaths),""});
            }
            else
            {
                resultTabbedPane.remove(paneBiology);
            }
            TestResultBean resultBean=new TestResultBean(testBean.getTestId(), totalPhysics, totalChemistry,  totalMaths, correctPhysics, correctChemistry,  correctMaths);            
        }
    }
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        resultTabbedPane = new javax.swing.JTabbedPane();
        panePhysics = new javax.swing.JScrollPane();
        tblPhysics = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        }
        ;
        paneChemistry = new javax.swing.JScrollPane();
        tblChemistry = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        };
        paneBiology = new javax.swing.JScrollPane();
        tblBiology = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        };
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnHome = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CruncherSoft's NEET Client Application");
        setResizable(false);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setBackground(new java.awt.Color(196, 223, 254));
        jPanel1.setName("jPanel1"); // NOI18N

        jPanel2.setBackground(new java.awt.Color(196, 223, 254));
        jPanel2.setName("jPanel2"); // NOI18N

        resultTabbedPane.setFont(new java.awt.Font("Century", 1, 11)); // NOI18N
        resultTabbedPane.setName("resultTabbedPane"); // NOI18N

        panePhysics.setFont(new java.awt.Font("Century", 1, 11)); // NOI18N
        panePhysics.setName("panePhysics"); // NOI18N

        ((DefaultTableCellRenderer)tblPhysics.getTableHeader().getDefaultRenderer())
        .setHorizontalAlignment(JLabel.CENTER);
        tblPhysics.setFont(new java.awt.Font("Verdana", 1, 15)); // NOI18N
        tblPhysics.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Question No.", "User Answer", "Result"
            }
        ));
        tblPhysics.setToolTipText("Double Click To View Detail Result.");
        tblPhysics.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblPhysics.setName("tblPhysics"); // NOI18N
        tblPhysics.setSelectionForeground(new java.awt.Color(204, 0, 0));
        tblPhysics.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblPhysics.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPhysicsMouseClicked(evt);
            }
        });
        panePhysics.setViewportView(tblPhysics);

        resultTabbedPane.addTab("Physics", panePhysics);

        paneChemistry.setFont(new java.awt.Font("Century", 1, 11)); // NOI18N
        paneChemistry.setName("paneChemistry"); // NOI18N

        ((DefaultTableCellRenderer)tblChemistry.getTableHeader().getDefaultRenderer())
        .setHorizontalAlignment(JLabel.CENTER);
        tblChemistry.setFont(new java.awt.Font("Verdana", 1, 15)); // NOI18N
        tblChemistry.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Question No.", "User Answer", "Result"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tblChemistry.setToolTipText("Double Click To View Detail Result.");
        tblChemistry.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblChemistry.setName("tblChemistry"); // NOI18N
        tblChemistry.setSelectionForeground(new java.awt.Color(204, 0, 0));
        tblChemistry.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblChemistryMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tblChemistryMouseEntered(evt);
            }
        });
        paneChemistry.setViewportView(tblChemistry);

        resultTabbedPane.addTab("Chemistry", paneChemistry);

        paneBiology.setFont(new java.awt.Font("Century", 1, 11)); // NOI18N
        paneBiology.setName("paneBiology"); // NOI18N

        ((DefaultTableCellRenderer)tblBiology.getTableHeader().getDefaultRenderer())
        .setHorizontalAlignment(JLabel.CENTER);
        tblBiology.setFont(new java.awt.Font("Verdana", 1, 15)); // NOI18N
        tblBiology.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Question No.", "User Answer", "Result"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tblBiology.setToolTipText("Double Click To View Detail Result.");
        tblBiology.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblBiology.setName("tblBiology"); // NOI18N
        tblBiology.setSelectionForeground(new java.awt.Color(204, 0, 0));
        tblBiology.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblBiologyMouseClicked(evt);
            }
        });
        paneBiology.setViewportView(tblBiology);

        resultTabbedPane.addTab("Maths", paneBiology);

        jPanel2.add(resultTabbedPane);

        jPanel3.setBackground(new java.awt.Color(196, 223, 254));
        jPanel3.setName("jPanel3"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel1.setText("Double Click On Question No. To Get Result In Detail...!");
        jLabel1.setDebugGraphicsOptions(javax.swing.DebugGraphics.FLASH_OPTION);
        jLabel1.setName("jLabel1"); // NOI18N
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
        });

        btnHome.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        btnHome.setText("Home");
        btnHome.setName("btnHome"); // NOI18N
        btnHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHomeMouseExited(evt);
            }
        });
        btnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHomeActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        btnSave.setText("Save");
        btnSave.setName("btnSave"); // NOI18N
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Verdana", 1, 36)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("SCORECARD");
        jLabel2.setName("jLabel2"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 393, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 104, Short.MAX_VALUE)
                        .addComponent(btnSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnHome)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnHome, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSave, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void tblPhysicsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPhysicsMouseClicked
    if(evt.getClickCount()==2)
    {
        int row = tblPhysics.rowAtPoint(evt.getPoint());
        if(row<alPhysics.size())
        {
            QuestionBean questionBean = alPhysics.get(row);
            new QuestionAnswerForm(questionBean, row+1).setVisible(true);
        }
    }
}//GEN-LAST:event_tblPhysicsMouseClicked

private void tblChemistryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblChemistryMouseClicked
    if(evt.getClickCount()==2)
    {
        int row = tblChemistry.rowAtPoint(evt.getPoint());
        if(row<alChemistry.size())
        {
            QuestionBean questionBean = alChemistry.get(row);
             new QuestionAnswerForm(questionBean, row+1).setVisible(true);
        }
    }    
}//GEN-LAST:event_tblChemistryMouseClicked

private void tblBiologyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblBiologyMouseClicked
    if(evt.getClickCount()==2)
    {
        int row = tblBiology.rowAtPoint(evt.getPoint());
        if(row<alBiology.size())
        {
            QuestionBean questionBean = alBiology.get(row);
            //JOptionPane.showMessageDialog(null,questionBean.getQuestion());
              new QuestionAnswerForm(questionBean, row+1).setVisible(true);
        }
    }        
}//GEN-LAST:event_tblBiologyMouseClicked

private void tblChemistryMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblChemistryMouseEntered
// TODO add your handling code here:
}//GEN-LAST:event_tblChemistryMouseEntered

private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
    jLabel1.setBackground(Color.red);
    jLabel1.setForeground(Color.BLUE);
}//GEN-LAST:event_jLabel1MouseEntered

private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
    jLabel1.setBackground(Color.BLUE);
    jLabel1.setForeground(Color.red);
}//GEN-LAST:event_jLabel1MouseExited

private void btnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHomeActionPerformed
    new NewTestForm(rollNo).setVisible(true);
    this.dispose();
}//GEN-LAST:event_btnHomeActionPerformed

private void btnHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMouseEntered
    btnHome.setForeground(Color.red);
}//GEN-LAST:event_btnHomeMouseEntered

private void btnHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMouseExited
    btnHome.setForeground(Color.black);
}//GEN-LAST:event_btnHomeMouseExited

private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
    Object[] options = { "YES", "CANCEL" };
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Save Test", "Warning",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
    if(i==0)
    {
        int id=testsBean.getTestId();
        String testName1=JOptionPane.showInputDialog(null, "Enter Name of test.");
        System.out.print(testName1);
        String testName=testName1+" (Test "+id+")";
            testsBean.setRemainingTime(60000000);
            testsBean.setGroupId(testsBean.getGroupId());
            testsBean.setCurrentQuestionNumber(0);
            SaveAllTests saveTest=new SaveAllTests();
            saveTest.saveTestBean(testsBean,testName,rollNo);
            
            new NewTestForm(rollNo).setVisible(true);
            this.dispose();
    }
}//GEN-LAST:event_btnSaveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(TestResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(TestResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(TestResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(TestResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
//                new TestResultForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnHome;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane paneBiology;
    private javax.swing.JScrollPane paneChemistry;
    private javax.swing.JScrollPane panePhysics;
    private javax.swing.JTabbedPane resultTabbedPane;
    private javax.swing.JTable tblBiology;
    private javax.swing.JTable tblChemistry;
    private javax.swing.JTable tblPhysics;
    // End of variables declaration//GEN-END:variables
}