package ui;

import java.awt.Color;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import JEE.practice.*;
import JEE.test.TestBean;
import JEE.unitTest.*;
import javax.swing.ImageIcon;

public class ResumeTest extends javax.swing.JFrame {
    ArrayList<Object> obj=new ArrayList<Object>();
    ArrayList<TestBean> testBean1;
    ArrayList<PracticeBean> practiceBean1;
    ArrayList<UnitTestBean> unitTestBean1;
    ArrayList<Integer> testIds1;
    java.awt.event.MouseEvent evt111;
    int subjectId,chapterId, i = 0;
    Calendar cal=Calendar.getInstance();
    Calendar cal1=Calendar.getInstance();
    int rollNo;
    Connection con1;
    /** Creates new form TestResultForm */
    public ResumeTest(int rollNo) {
        initComponents();
        this.rollNo=rollNo;
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
    }

    public void tablesetting()
    {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        tblPractice.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
        tblPractice.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
    }
    
    public ResumeTest(ArrayList<UnitTestBean> testBean,ArrayList<String> testDate,ArrayList<Integer> testIds,int im,int rollNo) {
        initComponents();
        this.rollNo=rollNo;
            testIds1=testIds;
            unitTestBean1=testBean;            
            String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
            tablesetting();
            setExtendedState(JFrame.MAXIMIZED_BOTH);        
            if(unitTestBean1!=null) {
            JEE.unitTest.DBConnection con=new JEE.unitTest.DBConnection();
            int j=unitTestBean1.size();
            if(j>0) {                
                DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();
                cal.add(Calendar.DATE, -1);
                cal1.add(Calendar.DATE, 0);
                Date yesterday=new Date(cal.getTimeInMillis());
                Date today=new Date(cal1.getTimeInMillis());
                String yester=yesterday.toString();
                String todays=today.toString();
                model.addRow(new Object[]{"",""});
                while(i<j) {
                    int id=i+1;
                    if(testDate.get(i).contains(yester)){
                        model.addRow(new Object[]{"Test"+id,"Yesterday"});                        
                    }
                    else if(testDate.get(i).contains(todays)){                       
                        model.addRow(new Object[]{"Test"+id,"Today"});                        
                    }
                    else{                 
                        model.addRow(new Object[]{"Test"+id,testDate.get(i)});                        
                    }
                    i++;
                }
                model.addRow(new Object[]{"",""});
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "No Test For Resume");
        }
    }
    
    public ResumeTest(ArrayList<TestBean> testBean,ArrayList<String> testDate,ArrayList<Integer> testIds,int rollNo) {
//        initComponents();
        this.rollNo=rollNo;
        testIds1=testIds;
        testBean1=testBean;
        tablesetting();
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        setExtendedState(JFrame.MAXIMIZED_BOTH);       
        if(testBean1!=null) {
            JEE.unitTest.DBConnection con=new JEE.unitTest.DBConnection();
            int y = 0;
            int j=testBean1.size();
            if(j>0) {
                DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();
                cal.add(Calendar.DATE, -1);
                cal1.add(Calendar.DATE, 0);
                Date yesterday=new Date(cal.getTimeInMillis());
                Date today=new Date(cal1.getTimeInMillis());
                String yester=yesterday.toString();
                String todays=today.toString();
                model.addRow(new Object[]{"",""});
                while(y<j) {
                    int id=y+1;
                    if(testDate.get(y).contains(yester)){
                        model.addRow(new Object[]{"Test"+id,"Yesterday"});                        
                    }
                    else if(testDate.get(y).contains(todays)){                       
                        model.addRow(new Object[]{"Test"+id,"Today"});                        
                    }
                    else{                 
                        model.addRow(new Object[]{"Test"+id,testDate.get(y)});                        
                    }
                    y++;
                }
                model.addRow(new Object[]{"",""});
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "No Test For Resume");
        }
    }
    
    public ResumeTest(ArrayList<PracticeBean> practiceBeans,ArrayList<String> testDate,ArrayList<Integer> testIds,boolean flag,int rollNo) {
//        initComponents();
        this.rollNo=rollNo;
        testIds1=testIds;
        practiceBean1=practiceBeans;
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        tablesetting();
        setExtendedState(JFrame.MAXIMIZED_BOTH);        
        if(practiceBean1!=null) {
            JEE.unitTest.DBConnection con=new JEE.unitTest.DBConnection();
            int j=practiceBean1.size();
            if(j>0) {                
                DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();
                cal.add(Calendar.DATE, -1);
                cal1.add(Calendar.DATE, 0);
                Date yesterday=new Date(cal.getTimeInMillis());
                Date today=new Date(cal1.getTimeInMillis());
                String yester=yesterday.toString();
                String todays=today.toString();
                model.addRow(new Object[]{"",""});
                while(i<j) {
                    int id=i+1;
                    if(testDate.get(i).contains(yester)){
                        model.addRow(new Object[]{"Test"+id,"Yesterday"});                        
                    }
                    else if(testDate.get(i).contains(todays)){                       
                        model.addRow(new Object[]{"Test"+id,"Today"});                        
                    }
                    else{                 
                        model.addRow(new Object[]{"Test"+id,testDate.get(i)});                        
                    }
                    i++;
                }
                model.addRow(new Object[]{"",""});
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "No Test For Resume");
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        panePhysics = new javax.swing.JScrollPane();
        tblPractice = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        }
        ;

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Scholars Katta's NEET+JEE Software 2014");
        setBackground(new java.awt.Color(196, 223, 254));

        jPanel2.setBackground(new java.awt.Color(196, 223, 254));
        jPanel2.setName("jPanel2"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 16));
        jLabel1.setText("Double Click On Test To Resume It...!");
        jLabel1.setName("jLabel1"); // NOI18N
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Times New Roman", 1, 16));
        jButton1.setText("Home");
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton1MouseExited(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(196, 223, 254));
        jPanel1.setFont(new java.awt.Font("Century", 1, 11));
        jPanel1.setName("jPanel1"); // NOI18N

        panePhysics.setBackground(new java.awt.Color(204, 255, 204));
        panePhysics.setFont(new java.awt.Font("Century", 1, 11));
        panePhysics.setName("panePhysics"); // NOI18N

        ((DefaultTableCellRenderer)tblPractice.getTableHeader().getDefaultRenderer())
        .setHorizontalAlignment(JLabel.CENTER);
        tblPractice.setAutoCreateRowSorter(true);
        tblPractice.setBackground(new java.awt.Color(176, 224, 230));
        tblPractice.setFont(new java.awt.Font("Verdana", 1, 14));
        tblPractice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TestName", "Date"
            }
        ));
        tblPractice.setToolTipText("Double Click To View Detail Result.");
        tblPractice.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblPractice.setGridColor(new java.awt.Color(0, 0, 0));
        tblPractice.setName("tblPractice"); // NOI18N
        tblPractice.setSelectionForeground(new java.awt.Color(204, 0, 0));
        tblPractice.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblPractice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPracticeMouseClicked(evt);
            }
        });
        panePhysics.setViewportView(tblPractice);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panePhysics, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panePhysics, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap(447, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(54, 54, 54)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void tblPracticeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPracticeMouseClicked
    if(evt.getClickCount()==2)
    {
        if(testBean1!=null)
        {
            int row = tblPractice.rowAtPoint(evt.getPoint());
            row--;
            if(row<=testBean1.size())
            {
                TestBean testBean = testBean1.get(row);
                JEE.test.DBConnection.delTest(testIds1.get(row));
                StartedTestForm t = new StartedTestForm(testBean,false,rollNo);
                
                t.setVisible(true);
                this.dispose();
            }
        }
        else if(practiceBean1!=null)
        {
            int row = tblPractice.rowAtPoint(evt.getPoint());
            row--;
            if(row<=practiceBean1.size())
            {
                PracticeBean testBean = practiceBean1.get(row);
                JEE.practice.DBConnection.delTest(testIds1.get(row));
                StartedPracticeForm t = new StartedPracticeForm(testBean,false,rollNo);
                t.setVisible(true);
                this.dispose();
            }
        }
        else if(unitTestBean1!=null)
        {
            int row = tblPractice.rowAtPoint(evt.getPoint());
            row--;
            if(row<=unitTestBean1.size())
            {
                UnitTestBean testBean = unitTestBean1.get(row);
                JEE.unitTest.DBConnection.delTest(testIds1.get(row));
                StartedUnitTestForm t = new StartedUnitTestForm(testBean,false,rollNo);
                t.setVisible(true);
                this.dispose();
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Test Cannot be Resumed");
        }
    }
}//GEN-LAST:event_tblPracticeMouseClicked

private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
    jLabel1.setBackground(Color.red);
    jLabel1.setForeground(Color.BLUE);
}//GEN-LAST:event_jLabel1MouseEntered

private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
    jLabel1.setBackground(Color.BLUE);
    jLabel1.setForeground(Color.red);
}//GEN-LAST:event_jLabel1MouseExited

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    new NewTestForm(rollNo).setVisible(true);
    this.dispose();
}//GEN-LAST:event_jButton1ActionPerformed

private void jButton1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseEntered
    jButton1.setForeground(Color.red);
}//GEN-LAST:event_jButton1MouseEntered

private void jButton1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseExited
    jButton1.setForeground(Color.black);
}//GEN-LAST:event_jButton1MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
//                new PracticeResultForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane panePhysics;
    private javax.swing.JTable tblPractice;
    // End of variables declaration//GEN-END:variables
 }