/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * RegisterStudentForm.java
 *
 * Created on Jun 15, 2013, 8:30:16 PM
 */
package ui;

import JEE.test.DBConnection;
import java.awt.Color;
import java.awt.event.InputEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import server.DBConnection1;
import server.IPAddress;

/**
 *
 * @author Avadhut
 */
public class LoginStudentForm extends javax.swing.JFrame {

    public String serverIP;
    int rollNo;

    /** Creates new form RegisterStudentForm */
    public LoginStudentForm() {
        initComponents();
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        getContentPane().setBackground(Color.white);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        setLocationRelativeTo(null);
        clearFields();
        
    }

    public void clearFields() {
        txtRollNo.setText("");
        txtPassword.setText("");

    }

    public void validateForm() {
        String pwd = txtPassword.getText();
        String rollno = txtRollNo.getText();
    }

    public String getIPAddress(String prefix) {
        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while (e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements()) {
                    Object object = ee.nextElement();
                    if (object instanceof Inet4Address) {
                        Inet4Address i = (Inet4Address) object;
                        String hostAddress = i.getHostAddress();
                        if (hostAddress.startsWith(prefix)) {
                            System.out.println(hostAddress);
                            return hostAddress;
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Logger.getLogger(LoginStudentForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean login() {
        String rollno = txtRollNo.getText();
        String pwd = txtPassword.getText();
        serverIP = server.Server.getServerIP();
        Connection connection = new DBConnection1().getClientConnection1(server.Server.getServerIP());
        try {
            if (connection != null) {
                int roll = Integer.parseInt(rollno);
                String sql = "Select * from Student_Info where rollno=? and pass=?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setInt(1, Integer.parseInt(rollno));
                preparedStatement.setString(2, pwd);
                ResultSet rs = preparedStatement.executeQuery();
//            System.out.println(rs.getString(1) + "," + rs.getString(2) + "," + rs.getString(3) + ",'" + rs.getString(4) + rs.getString(5) + ",'" + rs.getString(6));                
                if (rs.next()) {
                    String name = rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);
                    sql = "Select * from Network_Prefix";
                    preparedStatement = connection.prepareStatement(sql);
                    rs = preparedStatement.executeQuery();
                    String prefix = "192.168.2";
                    if (rs.next()) {
                        prefix = rs.getString(1);
                    }
                    String ipaddress = getIPAddress(prefix);
                    sql = "Select * from Client_Info where USER_ID=? and status=?";
                    preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setInt(1, roll);
                    preparedStatement.setBoolean(2, true);
                    rs = preparedStatement.executeQuery();
                    if (rs.next()) {
                        JOptionPane.showMessageDialog(rootPane, "Already Logged in");
                        return false;
                    } else {
//                        sql = "insert into Client_Info values(?,?,?,?)";
//                        preparedStatement = connection.prepareStatement(sql);
//                        preparedStatement.setString(1, ipaddress);
//                        preparedStatement.setInt(2, roll);
//                        preparedStatement.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
//                        preparedStatement.setBoolean(4, true);
//                        int executeUpdate = preparedStatement.executeUpdate();
//                        if (executeUpdate > 0) {
//                            this.rollNo = roll;
//                            JOptionPane.showMessageDialog(rootPane, "Success");
//                            Properties prop = new Properties();
//                            prop.setProperty("rollno", roll + "");
//                            prop.setProperty("serverIP", serverIP);
//                            prop.setProperty("name", name);
//                            saveProperties(prop);
//                            return true;
//                        } else {
//                            JOptionPane.showMessageDialog(rootPane, "Fail");
//                            return false;
//                        }
                    }
                }
            }else
            {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(LoginStudentForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return true;
    }

    public void saveProperties(Properties prop) {
        try {
            //save properties to project root folder
            prop.store(new FileOutputStream("config.properties"), null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public Properties getProperties() {
        Properties prop = new Properties();
        try {
            //load a properties file
            prop.load(new FileInputStream("config.properties"));

            //get the property value and print it out
            System.out.println(prop.getProperty("rollno"));
            System.out.println(prop.getProperty("name"));
            System.out.println(prop.getProperty("dbpassword"));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return prop;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtRollNo = new javax.swing.JTextField();
        txtPassword = new javax.swing.JPasswordField();
        btnLogin = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        lblServer = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Student Login");
        setUndecorated(true);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(29, 9, 44));
        jPanel1.setName("jPanel1"); // NOI18N

        jLabel10.setFont(new java.awt.Font("Segoe UI Semibold", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Student Login");
        jLabel10.setName("jLabel10"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addGap(11, 11, 11))
        );

        jPanel3.setBackground(new java.awt.Color(29, 9, 44));
        jPanel3.setName("jPanel3"); // NOI18N

        jLabel1.setBackground(new java.awt.Color(29, 9, 44));
        jLabel1.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("User Id:");
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel5.setBackground(new java.awt.Color(29, 9, 44));
        jLabel5.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Password :");
        jLabel5.setName("jLabel5"); // NOI18N

        txtRollNo.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        txtRollNo.setForeground(new java.awt.Color(29, 9, 44));
        txtRollNo.setName("txtRollNo"); // NOI18N

        txtPassword.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        txtPassword.setForeground(new java.awt.Color(29, 9, 44));
        txtPassword.setToolTipText("");
        txtPassword.setName("txtPassword"); // NOI18N

        btnLogin.setBackground(new java.awt.Color(255, 255, 255));
        btnLogin.setFont(new java.awt.Font("Segoe UI Semibold", 1, 16)); // NOI18N
        btnLogin.setForeground(new java.awt.Color(29, 9, 44));
        btnLogin.setText("Login");
        btnLogin.setName("btnLogin"); // NOI18N
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 255, 255));
        btnClear.setFont(new java.awt.Font("Segoe UI Semibold", 1, 16)); // NOI18N
        btnClear.setForeground(new java.awt.Color(29, 9, 44));
        btnClear.setText("Exit");
        btnClear.setName("btnClear"); // NOI18N
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        lblServer.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        lblServer.setForeground(new java.awt.Color(255, 0, 0));
        lblServer.setText("Update Server IP");
        lblServer.setName("lblServer"); // NOI18N
        lblServer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblServerMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btnLogin)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnClear))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel1))
                                .addGap(31, 31, 31)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtRollNo, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(26, 26, 26))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(lblServer)
                        .addContainerGap())))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(lblServer)
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtRollNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed

        System.exit(0);     }//GEN-LAST:event_btnClearActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        /*
        if (login()) {             Home home = new Home(rollNo);             home.setVisible(true);             this.dispose();         }     }//GEN-LAST:event_btnLoginActionPerformed
         */ 
        
            if (login()) {
            String pwd = txtPassword.getText();
            String rollno = txtRollNo.getText();
            System.out.println("rollno="+rollno);        
            int rollNo=Integer.parseInt(rollno);
            System.out.println("rollNo="+rollNo);
             try {
                    setUserId1(rollNo);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            new NewTestForm(rollNo).setVisible(true);
            this.dispose();
        }
            else
            {
                JOptionPane.showMessageDialog(null,"Please Start Server Connection...!!!");
            }
           
    }
    public void setUserId1(int rollNo) throws SQLException {
        Connection con = new DBConnection1().getClientConnection1(server.Server.getServerIP());
        Connection connection = new DBConnection1().getClientConnection1(server.Server.getServerIP());
                    String sql = "Select * from Network_Prefix";
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    ResultSet rs = preparedStatement.executeQuery();
                    String prefix = "192.168.2";
                    if (rs.next()) {
                        prefix = rs.getString(1);
                    }
                    String ipaddress = getIPAddress(prefix);
        
        String ClientPCName=new IPAddress().getIPAddress("192.168.2");
        
        System.out.println("ipaddress"+ipaddress);
        try {
            Statement s = con.createStatement();
            s.execute("update Client_Info set USER_ID= " + rollNo + " where CLIENTTCP_IP_ADDRESS='" + ipaddress+ "'");
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
       
    }
    

    private void lblServerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblServerMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new ConfigureServer().setVisible(true);
                break;
            }
        }
    }//GEN-LAST:event_lblServerMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(LoginStudentForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(LoginStudentForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(LoginStudentForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(LoginStudentForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new LoginStudentForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblServer;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtRollNo;
    // End of variables declaration//GEN-END:variables
}
