package JEE.practice;


import com.bean.ClassTestBean;
import com.bean.QuestionBean;
import com.bean.StudentBean;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import server.DBConnection1;
import server.Server;

public class DBConnection {

    String hintbtn;

    public DBConnection() {
    }
 
   
    public static void delTest(int testId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            String sql = "delete from Saved_Test_Info where id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i = ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public String getChapName(int id) {
        String name = null;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            String query = "Select Chapter_Name from Chapter_Info where Chapter_Id = " + id;
            rs = s.executeQuery(query);
            if (rs.next()) {
                name = rs.getString(1);
            }
            return name;
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return name;
    }

    public void addResult(PracticeResultBean practiceResultBean, int rollNo) {
        if (practiceResultBean != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            try {
                String sql = "insert into Practice_Result_Info values(?,?,?,?)";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setInt(1, practiceResultBean.getPracticeId());
                ps.setInt(2, rollNo);
                ps.setInt(3, practiceResultBean.getTotalQuestions());
                ps.setDouble(4, practiceResultBean.getCorrectQuestions());

                int r = ps.executeUpdate();
                if (r > 0) {
                    //System.out.println("Practice Result Added");
                } else {
                    //System.out.println("Problem in Result Adding");
                }
            } catch (Exception ex) {
                //JOptionPane.showMessageDialog(null, ex.getMessage());
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public double getPerwrongQuestionMarks(int subjectId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getDouble(1);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public double getPerQuestionMarks(int subjectId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select * from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getDouble(3);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public ArrayList<QuestionBean> selectQuestionsForPractice(int subjectId, int chapterId, int questionCount) {
        int cnt = 10;
        ArrayList<QuestionBean> al = getQuestionsForPractice(subjectId, chapterId);
        if (al != null) {
            Collections.shuffle(al);
            ArrayList<QuestionBean> alFinal = new ArrayList<QuestionBean>();
            for (int idx = 0; idx < cnt; ++idx) {
                alFinal.add(al.get(idx));
            }
            return alFinal;
        }
        return null;
    }
    private ArrayList<QuestionBean> getQuestionsForPractice(int subjectId, int chapterId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
            rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id =" + subjectId + " and Chapter_Id =" + chapterId + " order by Question_Id");
            while (rs1.next()) {
                String question, A, B, C, D, Answer, Hint, optionImagePath, QuestionImagePath, hintImagePath, qyear;
                int sub_Id, Chap_Id, Top_Id, question_Id, attempt, level, type;
                question_Id = rs1.getInt(1);
                question = rs1.getString(2);
                A = rs1.getString(3);
                B = rs1.getString(4);
                C = rs1.getString(5);
                D = rs1.getString(6);
                Answer = rs1.getString(7);
                Hint = rs1.getString(8);
                level = rs1.getInt(9);
                sub_Id = rs1.getInt(10);
                Chap_Id = rs1.getInt(11);
                Top_Id = rs1.getInt(12);
                QuestionImagePath = rs1.getString(14);
                optionImagePath = rs1.getString(16);
                hintImagePath = rs1.getString(18);
                boolean result = (rs1.getInt(13) == 0) ? false : true;
                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
                attempt = rs1.getInt(19);
                type = rs1.getInt(20);
                int view = 0;
                qyear = rs1.getString(21);
                QuestionBean q = new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, level, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath, result2, hintImagePath, attempt, type, qyear, view);
                al.add(q);
            }
            return al;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public PracticeBean getPracticeBean(int subjectId, int chapterId) {
        int questionCount = 0, totalTime = 0;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            String query = "Select * from Group_info where group_Id = " + subjectId;
            rs = s.executeQuery(query);
            if (rs.next()) {
                switch (subjectId) {
                    case 1:
                        questionCount = rs.getInt(3);
                        break;
                    case 2:
                        questionCount = rs.getInt(4);
                        break;
                    case 3:
                        questionCount = rs.getInt(5);
                        break;
                }
                totalTime = rs.getInt(6);
            }
            ArrayList<QuestionBean> alQuestions = selectQuestionsForPractice(subjectId, chapterId, questionCount);
            PracticeBean practiceBean = new PracticeBean();
            practiceBean.setSubjectId(subjectId);
            practiceBean.setChapterId(chapterId);
            practiceBean.setTotalTime(totalTime / 2);
            practiceBean.setRemainingTime(totalTime);
            practiceBean.setQuestions(alQuestions);
            return practiceBean;
        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

//    private ArrayList<QuestionBean> getQuestionsForPractice(int subjectId, int chapterId) {
//        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
//        ResultSet rs1;
//        try {
//            QuestionBean questionBean = new QuestionBean();
//            Statement s = conn.createStatement();
//            ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
//            rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id =" + subjectId + " and Chapter_Id =" + chapterId + " order by Question_Id");
//            while (rs1.next()) {
//                
//                questionBean.setQuestionId(rs1.getInt(1));
//                questionBean.setQuestion(rs1.getString(2));
//                questionBean.setOptionA(rs1.getString(3));
//                questionBean.setOptionB(rs1.getString(4));
//                questionBean.setOptionC(rs1.getString(5));
//                questionBean.setOptionD(rs1.getString(6));
//                questionBean.setAnswer(rs1.getString(7));
//                questionBean.setHint(rs1.getString(8));
//                questionBean.setLevel(rs1.getInt(9));
//                questionBean.setSubjectId(rs1.getInt(10));
//                questionBean.setChapterId(rs1.getInt(11));
//                questionBean.setTopicId(rs1.getInt(12));
//                questionBean.setQuestionImagePath(rs1.getString(14));
//                questionBean.setOptionImagePath(rs1.getString(16));
//                questionBean.setHintImagePath(rs1.getString(18));
//                boolean result = (rs1.getInt(13) == 0) ? false : true;
//                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
//                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
//                questionBean.setIsQuestionAsImage(result);
//                questionBean.setIsOptionAsImage(result1);
//                questionBean.setIsHintAsImage(result2);
//                
//                questionBean.setAttempt(rs1.getInt(19));
//                questionBean.setType(rs1.getInt(20));
//                int view = 0;
//                questionBean.setYear(rs1.getString(21));
//                
//                al.add(questionBean);
//            }
//            return al;
//        } catch (Exception e) {
//            //JOptionPane.showMessageDialog(null, e.getMessage());
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        return null;
//    }

    public String hintreturn() {
        return hintbtn;
    }

    public void finishPractice(int practiceId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            int r = s.executeUpdate("Update Practice_Info set status='Finished' where Practice_Id=" + practiceId);
            if (r > 0) {
                //System.out.println("Practice finished");
            }
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void saveStateOfNewPractice(PracticeBean practiceBean, int rollNo) {
        int practiceId = 0;
        if (practiceBean != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs;
            try {
                Statement s = conn.createStatement();
                rs = s.executeQuery("Select max(Practice_Id) from Practice_Info");
                if (rs.next()) {
                    practiceId = rs.getInt(1);
                }
                practiceId++;
                PreparedStatement ps = conn.prepareStatement("insert into Practice_Info values(?,?,?,?,?,?)");
                ps.setInt(1, practiceId);
                ps.setInt(2, rollNo);
                ps.setInt(3, practiceBean.getSubjectId());
                ps.setInt(4, practiceBean.getChapterId());
                ps.setString(5, "Started");
                ps.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
                int executeUpdate = ps.executeUpdate();
                practiceBean.setPracticeId(practiceId);
                ArrayList<QuestionBean> alQuestions = practiceBean.getQuestions();
                Iterator<QuestionBean> it = alQuestions.iterator();
                while (it.hasNext()) {
                    QuestionBean question = it.next();
                    ps = conn.prepareStatement("insert into Practice_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, practiceId);
                    ps.setInt(2, question.getQuestionId());
                    ps.setString(3, "unAttempted");
                    ps.executeUpdate();
                }
                practiceBean.setStatus("Started");
                practiceBean.setStartDateTime(new java.util.Date());
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void setQuestionStatus(QuestionBean question, int practiceId, String userAnswer) {
        if (question != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs1;
            try {
                Statement s = conn.createStatement();
                s.executeUpdate("update Practice_Question_Status_Info set user_Answer='" + userAnswer + "' where Question_Id=" + question.getQuestionId() + " and practice_Id=" + practiceId);

            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public String getResult(int practiceId, int rollNo,int Questionid ) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select user_Answer from  Practice_Question_Status_Info where practice_Id=" + practiceId + " and ROLLNO=" + rollNo+ "and QUESTION_ID="+Questionid);
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public static void main(String s1[]) {
        DBConnection s = new DBConnection();
    }

    public ArrayList<String> getClassTestInfo() {
        ArrayList<String> testInfo = new ArrayList<String>();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs, rs1, rs3;
        try {
            Statement s1 = conn.createStatement();
            Statement s = conn.createStatement();
            Statement s3 = conn.createStatement();
            rs1 = s1.executeQuery("Select test_Id from  Temp_Class_Test_Info");
            int tid = 0;
            if (rs1.next()) {
                tid = rs1.getInt(1);
            }

            rs3 = s3.executeQuery("select status from Test_And_Set where test_Id = " + tid);
            if (rs3.next()) {
                String geta = rs3.getString(1);
                if (geta.equals("Available")) {
                    rs = s.executeQuery("Select * from  Temp_Class_Test_Info");
                    if (rs.next()) {
                        String temp = rs.getInt(1) + "\t" + rs.getBoolean(3) + "\t" + rs.getTimestamp(4);
                        testInfo.add(temp);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testInfo;
    }

    public int getMarks(int index) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select Marks_Per_Question from  Subject_Info where Subject_Id=" + index);
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return -1;
    }
    
    public ArrayList<ClassTestBean> getClassTestInfo1() {
        ArrayList<ClassTestBean> classTestList = new ArrayList<ClassTestBean>();
        ClassTestBean classTestBean=null;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs, rs1, rs3;
        try {
            Statement s1 = conn.createStatement();
            Statement s = conn.createStatement();
            Statement s3 = conn.createStatement();
            rs1 = s1.executeQuery("Select test_Id from  Temp_Class_Test_Info");
            int tid = 0;
            if (rs1.next()) {
                tid = rs1.getInt(1);
            }

            rs3 = s3.executeQuery("select status from Test_And_Set where test_Id = " + tid);
            if (rs3.next()) {
                String geta = rs3.getString(1);
                if (geta.equals("Available")) {
                    rs = s.executeQuery("Select * from  Temp_Class_Test_Info");
                    if (rs.next()) {
            
                classTestBean=new ClassTestBean();
                classTestBean.setTestId(rs.getInt(1));
                classTestBean.setGroupId(rs.getInt(2));
                classTestBean.setStatus(rs.getString(3));
                classTestBean.setTestDate(rs.getTimestamp(4).toString());
                classTestBean.setTestTotalTime(rs.getInt(5));
                classTestBean.setTestName(rs.getString(6));
                classTestBean.setAcademicYear(rs.getString(7));
                classTestBean.setClass_Std(rs.getString(8));
                classTestBean.setDivission(rs.getString(9));
                classTestBean.setSTART_DATE(rs.getString(10));
                classTestBean.setEXPIRE_DATE(rs.getString(11));
                classTestList.add(classTestBean);
                return classTestList;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return classTestList;
    }

    public int getQuestionCount(int rollno) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        System.out.println("***rollno+" +rollno);
        ResultSet rs, rs1,rs2;
        String RollNO=Integer.toString(rollno);
        int RollNO1=0;
        String str = "";
        int QueCount=0;
        try {
            Statement s = conn.createStatement();
            Statement s1 = conn.createStatement();
            Statement s2 = conn.createStatement();
            rs2= s1.executeQuery("Select * from Student_Info where FULLNAME='" + RollNO+ "'");
            if (rs2.next()) {
                RollNO1 = rs2.getInt(1);
            }
            System.out.println("***RollNO+" +RollNO);
            rs1 = s1.executeQuery("select setInitials from Set_Rollno_Distribution where rollno = " + RollNO1);
            if (rs1.next()) {
                str = rs1.getString(1);
            }
            
            rs = s.executeQuery("Select count(*) from  Temp_Class_Test_Question_Info where setInitials='" + str+ "'");
           
            while (rs.next()) {
                QueCount= rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return QueCount;
    }

    public String getInitial(int rollNo) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());      
        
        String RollNO1=Integer.toString(rollNo);
        int RollNO2=0;
        ResultSet rs1,rs2;
        String str = "";
        try {
            Statement s2 = conn.createStatement();
            rs2= s2.executeQuery("Select * from Student_Info where FULLNAME='" + RollNO1+ "'");
            if (rs2.next()) {
                RollNO2 = rs2.getInt(1);
            }
            Statement s1 = conn.createStatement();
            rs1 = s1.executeQuery("select setInitials from Set_Rollno_Distribution where rollno = " + RollNO2);
            if (rs1.next()) {
                str = rs1.getString(1);
            }
            return str;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return str;
    }

    public int getTestTime() {
        int testInfo = 0;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs, rs1, rs3;
        try {
            Statement s1 = conn.createStatement();
            Statement s = conn.createStatement();
            Statement s3 = conn.createStatement();
            rs1 = s1.executeQuery("Select test_Id from  Temp_Class_Test_Info");
            int tid = 0;
            if (rs1.next()) {
                tid = rs1.getInt(1);
            }

            rs3 = s3.executeQuery("select testTime from Temp_Class_Test_Info where test_Id = " + tid);
            if (rs3.next()) {
                testInfo = rs3.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testInfo;
    }
    
    public String getStudentName(int rollNo) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());      
        
        String RollNO1=Integer.toString(rollNo);
        int RollNO2=0;
        ResultSet rs1,rs2;
        String str = "";
        try {
            Statement s2 = conn.createStatement();
            rs2= s2.executeQuery("Select * from Student_Info where FULLNAME='" + RollNO1+ "'");
            if (rs2.next()) {
                str=rs2.getString(7);
                
            }
              return str; 
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return str;
    }
    public String getTestName(int Test_Id) {
        String testName = null;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            Statement stmt = conn.createStatement();
            ResultSet   rs = stmt.executeQuery("Select * from Class_Test_Info where test_Id="+Test_Id);
                if (rs.next()) {
                    testName = rs.getString(6);
                    return testName;
                }
         
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testName;
}
public StudentBean getStudentInfo(int rollNo) {
        String RollNO=Integer.toString(rollNo);
        StudentBean studentBean=null;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s1 = conn.createStatement();
            
            rs = s1.executeQuery("select * from Student_Info where FULLNAME='" + RollNO+ "'" );
            if (rs.next()) {
            
                studentBean=new StudentBean();
                studentBean.setRollno(rs.getInt(1));
                studentBean.setFullname(rs.getString(2));
                studentBean.setPass(rs.getString(3));
                studentBean.setStandard(rs.getString(4));
                studentBean.setDivision(rs.getString(5));
                studentBean.setAcademicYear(rs.getString(6));
                studentBean.setStudentName(rs.getString(7));
                studentBean.setMobileNo(rs.getString(8));
               
                return studentBean;
            }
                
            
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return studentBean;
    }    
    
}