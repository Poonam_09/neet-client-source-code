package JEE.test;

import JEE.unitTest.UnitTestBean;
import com.bean.ClassTestBean;
import com.bean.QuestionBean;
import com.bean.RankBean;
import dbserver1.StudentBean;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.util.*;
import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.*;

import server.DBConnection1;
import server.IPAddress;
import server.Server;
import ui.LoginStudentForm;

public class DBConnection {

//    private static  String driver = "org.apache.derby.jdbc.EmbeddedDriver";
//    private static  String protocol = "jdbc:derby:CETJeeNew;create=true;dataEncryption=true;bootPassword=*007_Cruncher+Soft@Wagholi_007*";
    int chaptercount;
    ArrayList<Integer> E = new ArrayList<Integer>();
     private Connection con=null;
     private Statement s=null;
     private ResultSet rs=null,rs1=null;
     private PreparedStatement ps=null;
    

    public static void delAllTest(int testId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            String sql = "delete from Saved_All_Test where savedId=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i = ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void delTest(int testId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            String sql = "delete from Saved_Test_Info where id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, testId);
            int i = ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public int getPerwrongQuestionMarks(int subjectId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public int getPerQuestionMarks(int subjectId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select Marks_Per_Question from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public ArrayList<String> nameOfChap(int sub_id) {
        ArrayList<String> al = new ArrayList<String>();
        String cname;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("select Chapter_Name from chapter_info where subject_id =" + sub_id);
            while (rs1.next()) {
                cname = rs1.getString(1);
                al.add(cname);
            }
            return al;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public int noOfChap(int sub_id) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("select count(chapter_id) from chapter_info where subject_id =" + sub_id);
            while (rs1.next()) {
                chaptercount = rs1.getInt(1);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return chaptercount;
    }

    public void resetQuestionAttempt(int subjectId, int questionCount) {
        int count = 0;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("Select count(*) from Question_Info where Subject_Id =" + subjectId + "and  AttemptedValue = 0");
            if (rs1.next()) {
                count = rs1.getInt(1);
            }
            if (count < questionCount) {
                s.executeUpdate("Update Question_Info set AttemptedValue = 0 where Subject_Id =" + subjectId);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public ArrayList<QuestionBean> selectQuestionsForTest(int subjectId, int questionCount) {
        resetQuestionAttempt(subjectId, questionCount);
        ArrayList<QuestionBean> al = getQuestionsForTest(subjectId);
        if (al != null) {
            Collections.shuffle(al);
            ArrayList<QuestionBean> alFinal = new ArrayList<QuestionBean>();
            for (int idx = 0; idx < questionCount; ++idx) {
                alFinal.add(al.get(idx));
            }
            return alFinal;
        }
        return null;
    }

    public ArrayList<Integer> getQuestionCount(int groupId) {
        ArrayList<Integer> al = new ArrayList<Integer>();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select * from Group_Info where Group_Id = " + groupId);
            if (rs.next()) {
                al.add(rs.getInt(3));
                al.add(rs.getInt(4));
                al.add(rs.getInt(5));
                al.add(rs.getInt(6));
            }
            return al;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public ArrayList<QuestionBean> getchapno(ArrayList<String> nameofchap) {
        ArrayList<Integer> noofchaps = null;
        ArrayList<QuestionBean> questionBean1 = getQuestionsForUnitTest(noofchaps);
        return questionBean1;
    }

    public TestBean getTest(int groupId) {
        int physicsCount = 0, chemistryCount = 0, MathsCount = 0, totalTime = 0;

        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            String query = "Select * from Group_info where group_Id = " + groupId;
            rs = s.executeQuery(query);
            if (rs.next()) {
                physicsCount = rs.getInt(3);
                chemistryCount = rs.getInt(4);
                MathsCount = rs.getInt(5);
                totalTime = rs.getInt(6);
            }

            ArrayList<QuestionBean> alPhysics = selectQuestionsForTest(1, physicsCount);
            ArrayList<QuestionBean> alChemistry = selectQuestionsForTest(2, chemistryCount);
            ArrayList<QuestionBean> alMaths = selectQuestionsForTest(3, MathsCount);
            TestBean testBean = new TestBean();
            testBean.setGroupId(groupId);
            testBean.setTotalTime(totalTime);
            testBean.setRemainingTime(totalTime);
            testBean.setAlPhysics(alPhysics);
            testBean.setAlChemistry(alChemistry);
            testBean.setAlMaths(alMaths);

            return testBean;

        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public ArrayList<TestResultForGraph> getResultObjects(int rollNo) {
        ArrayList<TestResultForGraph> testResultForGraphs = new ArrayList<TestResultForGraph>();
        int count = 0;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            String query1 = "Select count(*) from Test_Result_Info where rollNo=" + rollNo;
            rs = s.executeQuery(query1);
            if (rs.next()) {
                count = rs.getInt(1);
            }
            if (count > 10) {
                count = count - 10;
            } else {
                count = 0;
            }
            String query = "Select * from Test_Result_Info where rollNo=" + rollNo + " order by test_Id";
            rs = s.executeQuery(query);
            int i = 0;
            while (rs.next()) {
                if (i >= count) {
                    TestResultForGraph testResult = new TestResultForGraph();
                    testResult.setTest_Id(rs.getInt(1));
                    testResult.setSubject_Id(rs.getInt(3));
                    testResult.setTotal_Questions(rs.getInt(4));
                    testResult.setCorrect_Questions(rs.getInt(5));
                    testResultForGraphs.add(testResult);
                }
                i++;
            }
            return testResultForGraphs;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return testResultForGraphs;
    }

    public ArrayList<Integer> getSubjectResultData(int subjectId, int rollNo) {
        int count = 0;
        ArrayList<Integer> al = new ArrayList<Integer>();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            String query2 = "Select count(*) from Test_Result_Info where subject_Id = " + subjectId + " and rollNo=" + rollNo;
            rs = s.executeQuery(query2);
            if (rs.next()) {
                count = rs.getInt(1);
            }
            if (count > 10) {
                count = count - 10;
            } else {
                count = 0;
            }
            int i = 0;
            String query = "Select * from Test_Result_Info where subject_Id = " + subjectId + " and rollNo=" + rollNo + " order by test_Id";
            rs = s.executeQuery(query);
            while (rs.next()) {
                if (i >= count) {
                    al.add(rs.getInt(5));
                }
                i++;
            }
            return al;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public Integer getMaxTestResId() {
        int id = 0;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            String query = "Select max(test_Id) from Test_Result_Info";
            rs = s.executeQuery(query);
            if (rs.next()) {
                id = rs.getInt(1);
            }
            return id;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return id;
    }

    public ArrayList<Object[]> getSubjectList() {
        ArrayList<Object[]> subjects = new ArrayList<Object[]>();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select * from Subject_Info");
            while (rs.next()) {
                subjects.add(new Object[]{rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4)});
            }
            return subjects;
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public void saveSubjects(java.util.Vector rows) {
        int testId = 0;
        if (rows != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs1;
            try {
                Statement s = conn.createStatement();
                int size = rows.size();
                PreparedStatement ps = conn.prepareStatement("Update Subject_Info set Marks_Per_Question = ?,Marks_Per_Wrong_Question = ? where Subject_Id = ?");
                for (int i = 0; i < size; i++) {
                    java.util.Vector row = (java.util.Vector) rows.elementAt(i);
                    int subjectId = (Integer) row.elementAt(0);
                    String subjectName = (String) row.elementAt(1);
                    int subjectMark = (Integer) row.elementAt(2);
                    int negativesubjectMark = (Integer) row.elementAt(3);

                    ps.setInt(1, subjectMark);
                    ps.setInt(3, subjectId);
                    ps.setInt(2, negativesubjectMark);

                    int r = ps.executeUpdate();
                    //System.out.println(r);
                }
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public LinkedHashMap<String, Integer> getChapterList(int Subject_Id) {
        LinkedHashMap<String, Integer> chapters = new LinkedHashMap<String, Integer>();
        ArrayList<String> al = new ArrayList<String>();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("Select * from Chapter_Info where Subject_Id=" + Subject_Id);
            while (rs1.next()) {
                chapters.put(rs1.getString(3), rs1.getInt(1));
            }
            return chapters;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public ArrayList<Object[]> getGroupList() {
        ArrayList<Object[]> groups = new ArrayList<Object[]>();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select * from Group_Info");
            while (rs.next()) {
                groups.add(new Object[]{rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6)});
            }
            return groups;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public ArrayList<Object[]> getsummary() {
        ArrayList<Object[]> summ = new ArrayList<Object[]>();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1, rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select * from Test_Info");
            rs1 = s.executeQuery("Select * from Test_Result_Info");
            while (rs.next()) {
                int i = 0;
                String grp = "-";
                i = rs.getInt(2);
                if (i == 1) {
                    grp = "Physics";
                } else if (i == 2) {
                    grp = "Chemistry";
                } else if (i == 3) {
                    grp = "Biology";
                } else if (i == 4) {
                    grp = "PCB";
                } else {
                    grp = "-";
                }
                summ.add(new Object[]{rs.getInt(1), grp, rs.getTimestamp(4), rs1.getInt(3), rs1.getInt(4)});
            }
            return summ;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public int saveGroups(java.util.Vector rows) {
        if (rows != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs1;
            try {
                Statement s = conn.createStatement();
                int size = rows.size();
                PreparedStatement ps = conn.prepareStatement("Update Group_Info set physics_Question_Count=?,Chemistry_Question_Count=?,Maths_Question_Count=?,Total_Minutes=? where group_Id = ?");
                for (int i = 0; i < size; i++) {
                    java.util.Vector row = (java.util.Vector) rows.elementAt(i);
                    int pc = (Integer) row.elementAt(2);
                    int cc = (Integer) row.elementAt(3);
                    int bc = (Integer) row.elementAt(4);
                    int time = (Integer) row.elementAt(5);
                    int groupId = (Integer) row.elementAt(0);


                    ps.setInt(5, groupId);
                    ps.setInt(1, pc);
                    ps.setInt(2, cc);//(Integer)row.elementAt(3)); 
                    ps.setInt(3, bc);//(Integer)row.elementAt(5)); 
                    ps.setInt(4, time);//(Integer)row.elementAt(6));
                    int r = ps.executeUpdate();
                }
                return 1;
            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return 0;
    }

    private ArrayList<QuestionBean> getQuestionsForTest(int Subject_Id) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            QuestionBean questionBean = new QuestionBean();
            Statement s = conn.createStatement();
            ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
            rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id =" + Subject_Id + "and  AttemptedValue = 0 order by Question_Id");
            while (rs1.next()) {
                questionBean.setQuestionId(rs1.getInt(1));
                questionBean.setQuestion(rs1.getString(2));
                questionBean.setOptionA(rs1.getString(3));
                questionBean.setOptionB(rs1.getString(4));
                questionBean.setOptionC(rs1.getString(5));
                questionBean.setOptionD(rs1.getString(6));
                questionBean.setAnswer(rs1.getString(7));
                questionBean.setHint(rs1.getString(8));
                questionBean.setLevel(rs1.getInt(9));
                questionBean.setSubjectId(rs1.getInt(10));
                questionBean.setChapterId(rs1.getInt(11));
                questionBean.setTopicId(rs1.getInt(12));
                questionBean.setQuestionImagePath(rs1.getString(14));
                questionBean.setOptionImagePath(rs1.getString(16));
                questionBean.setHintImagePath(rs1.getString(18));
                boolean result = (rs1.getInt(13) == 0) ? false : true;
                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
                questionBean.setIsQuestionAsImage(result);
                questionBean.setIsOptionAsImage(result1);
                questionBean.setIsHintAsImage(result2);
                
                questionBean.setAttempt(rs1.getInt(19));
                questionBean.setType(rs1.getInt(20));
                int view = 0;
                questionBean.setYear(rs1.getString(21));
                
                al.add(questionBean);
            }
            return al;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

//    private ArrayList<QuestionBean> getQuestionsForUnitTest(ArrayList<Integer> noofchaps) {
//        int size = noofchaps.size();
//        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
//        ResultSet rs1;
//        try {
//            QuestionBean questionBean = new QuestionBean();
//            Statement s = conn.createStatement();
//            ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
//            for (int i = 0; i < size; i++) {
//                int m = 0;
//                while (m != 10) {
//                    rs1 = s.executeQuery("Select distinct * from Question_Info where Chapter_Id =" + noofchaps.get(i) + "and  AttemptedValue = 0 order by Question_Id");
//                    while (rs1.next()) {
//                questionBean.setQuestionId(rs1.getInt(1));
//                questionBean.setQuestion(rs1.getString(2));
//                questionBean.setOptionA(rs1.getString(3));
//                questionBean.setOptionB(rs1.getString(4));
//                questionBean.setOptionC(rs1.getString(5));
//                questionBean.setOptionD(rs1.getString(6));
//                questionBean.setAnswer(rs1.getString(7));
//                questionBean.setHint(rs1.getString(8));
//                questionBean.setLevel(rs1.getInt(9));
//                questionBean.setSubjectId(rs1.getInt(10));
//                questionBean.setChapterId(rs1.getInt(11));
//                questionBean.setTopicId(rs1.getInt(12));
//                questionBean.setQuestionImagePath(rs1.getString(14));
//                questionBean.setOptionImagePath(rs1.getString(16));
//                questionBean.setHintImagePath(rs1.getString(18));
//                boolean result = (rs1.getInt(13) == 0) ? false : true;
//                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
//                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
//                questionBean.setIsQuestionAsImage(result);
//                questionBean.setIsOptionAsImage(result1);
//                questionBean.setIsHintAsImage(result2);
//                
//                questionBean.setAttempt(rs1.getInt(19));
//                questionBean.setType(rs1.getInt(20));
//                int view = 0;
//                questionBean.setYear(rs1.getString(21));
//                
//                al.add(questionBean);
//                    }
//                    m++;
//                }
//            }
//            return al;
//        } catch (Exception e) {
//            //JOptionPane.showMessageDialog(null, e.getMessage());
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        return null;
//    }

    public ArrayList<QuestionBean> selectQuestionsForPractice(int subjectId, int questionCount) {
        ArrayList<QuestionBean> al = getQuestionsForPractice(subjectId);
        if (al != null) {
            ArrayList<QuestionBean> alFinal = new ArrayList<QuestionBean>();
            int size = al.size();
            Random randomGenerator = new Random();
            for (int idx = 1; idx <= questionCount; ++idx) {
                int randomInt = randomGenerator.nextInt(size);
                alFinal.add(al.get(randomInt));
            }
            return alFinal;
        }
        return null;
    }

    public void finishTest(int testId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            int r = s.executeUpdate("Update Test_Info set status='Finished' where testId=" + testId);
            if (r > 0) {
                //System.out.println("Test finished");
            }
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void saveStateOfNewTest(TestBean testBean, int rollNo) {
        int testId = 0;
        if (testBean != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs;
            try {
                Statement s = conn.createStatement();
                rs = s.executeQuery("Select max(test_Id) from Test_Info");
                if (rs.next()) {
                    testId = rs.getInt(1);
                }
                testId++;

                java.util.Date date = new java.util.Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String strDate = formatter.format(date);

                PreparedStatement ps = conn.prepareStatement("insert into Test_Info values(?,?,?,?,?)");
                ps.setInt(1, testId);
                ps.setInt(2, rollNo);
                ps.setInt(3, testBean.getGroupId());
                ps.setString(4, "Started");
                ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
                ps.executeUpdate();

                testBean.setTestId(testId);

                ArrayList<QuestionBean> alPhysics = testBean.getAlPhysics();
                ArrayList<QuestionBean> alChemistry = testBean.getAlChemistry();
                ArrayList<QuestionBean> alMaths = testBean.getAlMaths();

                Iterator<QuestionBean> it = alPhysics.iterator();
                while (it.hasNext()) {
                    QuestionBean question = it.next();
                    ps = conn.prepareStatement("insert into Test_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, testId);
                    ps.setInt(2, question.getQuestionId());
                    ps.setString(3, "unAttempted");
                    ps.executeUpdate();
                    s.executeUpdate("update Question_Info set AttemptedValue=1 where Question_Id=" + question.getQuestionId());
                }

                it = alChemistry.iterator();
                while (it.hasNext()) {
                    QuestionBean question = it.next();
                    ps = conn.prepareStatement("insert into Test_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, testId);
                    ps.setInt(2, question.getQuestionId());
                    ps.setString(3, "unAttempted");
                    ps.executeUpdate();
                    s.executeUpdate("update Question_Info set AttemptedValue=1 where Question_Id=" + question.getQuestionId());
                }

                it = alMaths.iterator();
                while (it.hasNext()) {
                    QuestionBean question = it.next();
                    ps = conn.prepareStatement("insert into Test_Question_Status_Info values(?,?,?)");
                    ps.setInt(1, testId);
                    ps.setInt(2, question.getQuestionId());
                    ps.setString(3, "unAttempted");
                    ps.executeUpdate();
                    s.executeUpdate("update Question_Info set AttemptedValue=1 where Question_Id=" + question.getQuestionId());
                }
                testBean.setStatus("Started");
                testBean.setStartDateTime(new java.util.Date());
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void setQuestionStatus(QuestionBean question, int testId, String userAnswer) {
        if (question != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs1;
            try {
                Statement s = conn.createStatement();
                s.executeUpdate("update Test_Question_Status_Info set user_Answer='" + userAnswer + "' where Question_Id=" + question.getQuestionId() + " and test_Id=" + testId);
            } catch (SQLException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public String getResult(int testId, int questionId) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("Select user_Answer from  Test_Question_Status_Info where test_Id=" + testId + " and Question_Id=" + questionId);
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    private ArrayList<QuestionBean> getQuestionsForPractice(int Subject_Id) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            QuestionBean questionBean = new QuestionBean();
            Statement s = conn.createStatement();
            ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
            rs1 = s.executeQuery("Select * from Question_Info where Subject_Id =" + Subject_Id + " order by Question_Id");
            while (rs1.next()) {
               questionBean.setQuestionId(rs1.getInt(1));
                questionBean.setQuestion(rs1.getString(2));
                questionBean.setOptionA(rs1.getString(3));
                questionBean.setOptionB(rs1.getString(4));
                questionBean.setOptionC(rs1.getString(5));
                questionBean.setOptionD(rs1.getString(6));
                questionBean.setAnswer(rs1.getString(7));
                questionBean.setHint(rs1.getString(8));
                questionBean.setLevel(rs1.getInt(9));
                questionBean.setSubjectId(rs1.getInt(10));
                questionBean.setChapterId(rs1.getInt(11));
                questionBean.setTopicId(rs1.getInt(12));
                questionBean.setQuestionImagePath(rs1.getString(14));
                questionBean.setOptionImagePath(rs1.getString(16));
                questionBean.setHintImagePath(rs1.getString(18));
                boolean result = (rs1.getInt(13) == 0) ? false : true;
                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
                questionBean.setIsQuestionAsImage(result);
                questionBean.setIsOptionAsImage(result1);
                questionBean.setIsHintAsImage(result2);
                
                questionBean.setAttempt(rs1.getInt(19));
                questionBean.setType(rs1.getInt(20));
                int view = 0;
                questionBean.setYear(rs1.getString(21));
                
                al.add(questionBean);
            }
            return al;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public void addResult(TestResultBean resultBean, int rollNo) {
        if (resultBean != null) {
            Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs;
            try {
                Statement s = conn.createStatement();
                String sql = "insert into Test_Result_Info values(?,?,?,?,?)";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setInt(1, resultBean.getTestId());
                ps.setInt(2, rollNo);
                ps.setInt(3, 1);
                ps.setInt(4, resultBean.getTotalPhysics());
                ps.setInt(5, resultBean.getObtainedPhysics());
                int r = ps.executeUpdate();

                ps.setInt(1, resultBean.getTestId());
                ps.setInt(2, rollNo);
                ps.setInt(3, 2);
                ps.setInt(4, resultBean.getTotalChemistry());
                ps.setInt(5, resultBean.getObtainedChemistry());
                r = ps.executeUpdate();


                ps.setInt(1, resultBean.getTestId());
                ps.setInt(2, rollNo);
                ps.setInt(3, 3);
                ps.setInt(4, resultBean.getTotalMaths());
                ps.setInt(5, resultBean.getObtainedMaths());
                r = ps.executeUpdate();

                if (r > 0) {
                    //System.out.println("Result Added");
                } else {
                    //System.out.println("Problem in Result Adding");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(SaveAllTests.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void viewXYAreaGraph(int groupId, int rollNo) {
        XYSeries series = new XYSeries("Average Weight");
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            String subjectObtained = "", subjectTotal = "";
            switch (groupId) {
                case 1:
                    subjectObtained = "PhysicsObtained";
                    subjectTotal = "PhysicsTotal";
                    break;
                case 2:
                    subjectObtained = "ChemistryObtained";
                    subjectTotal = "ChemistryTotal";
                    break;
                case 3:
                    subjectObtained = "BiologyObtained";
                    subjectTotal = "BiologyTotal";
                    break;
            }
            String query = "SELECT " + subjectObtained + " FROM Test_Result_Info WHERE " + subjectTotal + " > 0 AND test_Id IN (SELECT distinct test_Id FROM test_info WHERE group_Id=" + groupId + " AND rollNo=" + rollNo + ") order by test_Id";
            rs1 = s.executeQuery(query);
            int i = 1;
            while (rs1.next()) {
                series.add(i, rs1.getInt(1));
                i++;
            }
            XYDataset xyDataset = new XYSeriesCollection(series);
            JFreeChart chart = ChartFactory.createXYAreaChart("XY Chart using JFreeChart", "Age", "Weight", xyDataset, PlotOrientation.VERTICAL, true,
                    true, false);
            ChartFrame frame1 = new ChartFrame("XYArea Chart", chart);
            frame1.setVisible(true);
            frame1.setSize(300, 300);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void main(String s1[]) {
        DBConnection s = new DBConnection();
//        s.createTables();
    }
    
    public UnitTestBean getClassTestBean1(int testId, String ini) {
        UnitTestBean unitTestBean = new UnitTestBean();
        String flag = null;
        ArrayList<Integer> queIds = new ArrayList<Integer>();
        ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("Select status from Temp_Class_Test_Info where test_Id=" + testId);
            if (rs1.next()) {
                flag = rs1.getString(1);
            }
            rs1 = s.executeQuery("Select question_Id from Temp_Class_Test_Question_Info where testId=" + testId + " and setInitials='" + ini + "'");
            while (rs1.next()) {
                int q = rs1.getInt(1);
                queIds.add(q);
            }
            for (int i = 0; i < queIds.size(); i++) {
                rs1 = s.executeQuery("Select * from Question_Info where Question_Id=" + queIds.get(i));
                if (rs1.next()) {
                    String question, A, B, C, D, Answer, Hint, optionImagePath, QuestionImagePath, hintImagePath, qyear;
                    int sub_Id, Chap_Id, Top_Id, question_Id, attempt, level, type;
                    question_Id = rs1.getInt(1);
                    question = rs1.getString(2);
                    A = rs1.getString(3);
                    B = rs1.getString(4);
                    C = rs1.getString(5);
                    D = rs1.getString(6);
                    Answer = rs1.getString(7);
                    Hint = rs1.getString(8);
                    level = rs1.getInt(9);
                    sub_Id = rs1.getInt(10);
                    Chap_Id = rs1.getInt(11);
                    Top_Id = rs1.getInt(12);
                    QuestionImagePath = rs1.getString(14);
                    optionImagePath = rs1.getString(16);
                    hintImagePath = rs1.getString(18);
                    boolean result = (rs1.getInt(13) == 0) ? false : true;
                    boolean result1 = (rs1.getInt(15) == 0) ? false : true;
                    boolean result2 = (rs1.getInt(17) == 0) ? false : true;
                    attempt = rs1.getInt(19);
                    type = rs1.getInt(20);
                    qyear = rs1.getString(21);
                    int view = 0;
                    QuestionBean q = new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, level, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath, result2, hintImagePath, attempt, type, qyear, view);
                    al.add(q);
                }
            }
           
            unitTestBean.setQuestions(al);
            unitTestBean.setStatus(flag);
            return unitTestBean;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return unitTestBean;
    }

//    public UnitTestBean getClassTestBean(int testId, String ini) {
//        System.out.println("testId=***" +testId);
//        UnitTestBean unitTestBean = new UnitTestBean();
//        String flag = null;
//        ArrayList<Integer> queIds = new ArrayList<Integer>();
//        ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
//        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
//        ResultSet rs1;
//        QuestionBean questionBean =null;
//        try {
//            questionBean = new QuestionBean();
//            Statement s = conn.createStatement();
//            rs1 = s.executeQuery("Select status from Temp_Class_Test_Info where test_Id=" + testId);
//            if (rs1.next()) {
//                flag = rs1.getString(1);
//            }
//            rs1 = s.executeQuery("Select question_Id from Temp_Class_Test_Question_Info where testId=" + testId + " and setInitials='" + ini + "'");
//            while (rs1.next()) {
//                int q = rs1.getInt(1);
//                queIds.add(q);
//            }
//            for (int i = 0; i < queIds.size(); i++) {
//                rs1 = s.executeQuery("Select * from Question_Info where Question_Id=" + queIds.get(i));
//                if (rs1.next()) {
//                questionBean.setQuestionId(rs1.getInt(1));
//                questionBean.setQuestion(rs1.getString(2));
//                questionBean.setOptionA(rs1.getString(3));
//                questionBean.setOptionB(rs1.getString(4));
//                questionBean.setOptionC(rs1.getString(5));
//                questionBean.setOptionD(rs1.getString(6));
//                questionBean.setAnswer(rs1.getString(7));
//                questionBean.setHint(rs1.getString(8));
//                questionBean.setLevel(rs1.getInt(9));
//                questionBean.setSubjectId(rs1.getInt(10));
//                questionBean.setChapterId(rs1.getInt(11));
//                questionBean.setTopicId(rs1.getInt(12));
//                questionBean.setQuestionImagePath(rs1.getString(14));
//                questionBean.setOptionImagePath(rs1.getString(16));
//                questionBean.setHintImagePath(rs1.getString(18));
//                boolean result = (rs1.getInt(13) == 0) ? false : true;
//                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
//                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
//                questionBean.setIsQuestionAsImage(result);
//                questionBean.setIsOptionAsImage(result1);
//                questionBean.setIsHintAsImage(result2);
//                
//                questionBean.setAttempt(rs1.getInt(19));
//                questionBean.setType(rs1.getInt(20));
//                int view = 0;
//                questionBean.setYear(rs1.getString(21));
//                    System.out.println("**QUe=" +questionBean.getQuestion());
//                    System.out.println("**QUeID=" +questionBean.getQuestionId());
//                al.add(questionBean);
//                }
//            }
//            unitTestBean.setUnitTestId(testId);
//            unitTestBean.setQuestions(al);
//            unitTestBean.setStatus(flag);
//            return unitTestBean;
//        } catch (SQLException ex) {
//            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        return unitTestBean;
//    }

    public TestBean getClassTest(int subjectId) {
        int physicsCount = 0, chemistryCount = 0, MathsCount = 0, totalTime = 0;

        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            String query = "Select * from Group_info where group_Id = " + subjectId;
            rs = s.executeQuery(query);
            if (rs.next()) {
                physicsCount = rs.getInt(3);
                chemistryCount = rs.getInt(4);
                MathsCount = rs.getInt(5);
                totalTime = rs.getInt(6);
            }

            ArrayList<QuestionBean> alPhysics = selectQuestionsForTest(1, physicsCount);
            ArrayList<QuestionBean> alChemistry = selectQuestionsForTest(2, chemistryCount);
            ArrayList<QuestionBean> alMaths = selectQuestionsForTest(3, MathsCount);
            TestBean testBean = new TestBean();
            testBean.setGroupId(subjectId);
            testBean.setTotalTime(totalTime);
            testBean.setRemainingTime(totalTime);
            testBean.setAlPhysics(alPhysics);
            testBean.setAlChemistry(alChemistry);
            testBean.setAlMaths(alMaths);

            return testBean;

        } catch (Exception ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public String getChapterName(int chapterId) {
        String chapterName = "";
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = conn.createStatement();
            String query = "Select Chapter_Name from Chapter_Info where Chapter_Id=" + chapterId;
            rs = s.executeQuery(query);
            if (rs.next()) {
                chapterName = rs.getString(1);
                return chapterName;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

//    public ArrayList<QuestionBean> getAllQuestions(int subjectId, int chapterIds) {
//        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
//        ResultSet rs1;
//        try {
//            QuestionBean questionBean = new QuestionBean();
//            Statement s = con.createStatement();
//            rs1 = s.executeQuery("Select distinct * from Question_Info where Subject_Id =" + subjectId + " and Chapter_Id =" + chapterIds + " order by Question_Id");
//            ArrayList<QuestionBean> altemp = new ArrayList<QuestionBean>();
//            while (rs1.next()) {
//                questionBean.setQuestionId(rs1.getInt(1));
//                questionBean.setQuestion(rs1.getString(2));
//                questionBean.setOptionA(rs1.getString(3));
//                questionBean.setOptionB(rs1.getString(4));
//                questionBean.setOptionC(rs1.getString(5));
//                questionBean.setOptionD(rs1.getString(6));
//                questionBean.setAnswer(rs1.getString(7));
//                questionBean.setHint(rs1.getString(8));
//                questionBean.setLevel(rs1.getInt(9));
//                questionBean.setSubjectId(rs1.getInt(10));
//                questionBean.setChapterId(rs1.getInt(11));
//                questionBean.setTopicId(rs1.getInt(12));
//                questionBean.setQuestionImagePath(rs1.getString(14));
//                questionBean.setOptionImagePath(rs1.getString(16));
//                questionBean.setHintImagePath(rs1.getString(18));
//                boolean result = (rs1.getInt(13) == 0) ? false : true;
//                boolean result1 = (rs1.getInt(15) == 0) ? false : true;
//                boolean result2 = (rs1.getInt(17) == 0) ? false : true;
//                questionBean.setIsQuestionAsImage(result);
//                questionBean.setIsOptionAsImage(result1);
//                questionBean.setIsHintAsImage(result2);
//                
//                questionBean.setAttempt(rs1.getInt(19));
//                questionBean.setType(rs1.getInt(20));
//                int view = 0;
//                questionBean.setYear(rs1.getString(21));
//                
//                altemp.add(questionBean);
//            }
//            return altemp;
//        } catch (Exception e) {
//        } finally {
//            if (con != null) {
//                try {
//                    con.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        return null;
//    }

    public int checkpass(String pass, int roll) {
        String str = null;
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            rs = s.executeQuery("select * from Student_Info where rollno=" + roll);
            if (rs.next()) {
                str = rs.getString(3);
            }
            if (str.equals(pass)) {
                return 1;
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public int changepass(String cpass, int roll) {
        String str = null;
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        try {
            Statement s = con.createStatement();
            s.execute("update Student_Info set pass='" + cpass + "' where rollno=" + roll);
            return 1;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

     public int logout(int rollNo) throws SQLException {
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
//        Connection connection = new DBConnection1().getClientConnection1(server.Server.getServerIP());
//                    String sql = "Select * from Network_Prefix";
//                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
//                    ResultSet rs = preparedStatement.executeQuery();
//                    String prefix = "192.168.2";
//                    if (rs.next()) {
//                        prefix = rs.getString(1);
//                    }
//                    String ipaddress = getIPAddress(prefix);
//        
//        String ClientPCName=new IPAddress().getIPAddress("192.168.2");
//        
//        System.out.println("ipaddress"+ipaddress);
        try {
            Statement s = con.createStatement();
            s.execute("update Client_Info set status='false' where USER_ID= " + rollNo);
            return 1;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 1;
    }
    public String getIPAddress(String prefix) {
        try {
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while (e.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements()) {
                    Object object = ee.nextElement();
                    if (object instanceof Inet4Address) {
                        Inet4Address i = (Inet4Address) object;
                        String hostAddress = i.getHostAddress();
                        if (hostAddress.startsWith(prefix)) {
                            System.out.println(hostAddress);
                            return hostAddress;
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Logger.getLogger(LoginStudentForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public StudentBean getStudentInfo(int rollNo) {
        String str = null;
        Connection con = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs, rs1;
        try {
            StudentBean sb = new StudentBean();
            Statement s = con.createStatement();
            Statement s1 = con.createStatement();
            rs = s.executeQuery("select * from Student_Info where rollno=" + rollNo);
            rs1 = s1.executeQuery("select * from Client_Info where rollno=" + rollNo);
            if (rs.next() && rs1.next()) {
                sb.setRoll(rollNo);
                sb.setName(rs.getString(2));
                sb.setStd(rs.getString(4));
                sb.setDiv(rs.getString(5));
                sb.setIp(rs1.getString(1));
                sb.setConnectedTime(rs1.getString(3));
                sb.setYear(rs.getString(6));
                sb.setStatus(rs1.getBoolean(4));
            }
            return sb;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    public int getClassTestTime(int testId) {
        int time = 0;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("Select testTime from Class_Test_Info where test_Id=" + testId);
            if (rs1.next()) {
                time = rs1.getInt(1);
            }
            return time;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return time;
    }

    public int checkStatus(int rollNo) {//CurrentTestStatusOfStudent
        int status = 0;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("Select * from CurrentTestStatusOfStudent where rollno=" + rollNo);
            if (rs1.next()) {
                status = rs1.getInt(2);
            }
            return status;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return status;
    }

    public void setStatus(int rollNo) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            Statement s1 = conn.createStatement();
            rs1 = s.executeQuery("Select * from CurrentTestStatusOfStudent where rollno=" + rollNo);
            if (rs1.next()) {
                s1.executeUpdate("update CurrentTestStatusOfStudent set statusoftest=0 where rollno=" + rollNo);
            } else {
                s1.execute("insert into CurrentTestStatusOfStudent values(" + rollNo + ",0)");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public int getSubjectIdofTest(int testId1) {
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        int ret = 0;
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("Select group_Id from Temp_Class_Test_Info where test_Id=" + testId1);
            if (rs1.next()) {
                ret = rs1.getInt(1);
            }
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }
    
    private ArrayList<QuestionBean> getQuestionsForUnitTest(ArrayList<Integer> noofchaps) {
        int size = noofchaps.size();
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            ArrayList<QuestionBean> al = new ArrayList<QuestionBean>();
            for (int i = 0; i < size; i++) {
                int m = 0;
                while (m != 10) {
                    rs1 = s.executeQuery("Select distinct * from Question_Info where Chapter_Id =" + noofchaps.get(i) + "and  AttemptedValue = 0 order by Question_Id");
                    while (rs1.next()) {
                        String question, A, B, C, D, Answer, Hint, optionImagePath, QuestionImagePath, hintImagePath, qyear;
                        int sub_Id, Chap_Id, Top_Id, question_Id, attempt, level, type;
                        question_Id = rs1.getInt(1);
                        question = rs1.getString(2);
                        A = rs1.getString(3);
                        B = rs1.getString(4);
                        C = rs1.getString(5);
                        D = rs1.getString(6);
                        Answer = rs1.getString(7);
                        Hint = rs1.getString(8);
                        level = rs1.getInt(9);
                        sub_Id = rs1.getInt(10);
                        Chap_Id = rs1.getInt(11);
                        Top_Id = rs1.getInt(12);
                        QuestionImagePath = rs1.getString(14);
                        optionImagePath = rs1.getString(16);
                        hintImagePath = rs1.getString(18);
                        boolean result = (rs1.getInt(13) == 0) ? false : true;
                        boolean result1 = (rs1.getInt(15) == 0) ? false : true;
                        boolean result2 = (rs1.getInt(17) == 0) ? false : true;
                        attempt = rs1.getInt(19);
                        type = rs1.getInt(20);
                        qyear = rs1.getString(21);
                        int view = 0;
                        QuestionBean q = new QuestionBean(question_Id, question, A, B, C, D, Answer, Hint, level, sub_Id, Chap_Id, Top_Id, result, QuestionImagePath, result1, optionImagePath, result2, hintImagePath, attempt, type, qyear, view);
                        al.add(q);
                    }
                    m++;
                }
            }
            return al;
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }
    
    public String getStudentName(int SUBJECTID) {
        String SubjectName = null;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("Select * from SUBJECT_INFO where SUBJECT_ID=" + SUBJECTID);
            if (rs1.next()) {
                SubjectName = rs1.getString(2);
            }
            return SubjectName;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return SubjectName;
    }
    
     public ClassTestBean getClassTestInfo1() {
        
        ClassTestBean classTestBean=null;
        Connection conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs, rs1, rs3;
        try {
            Statement s1 = conn.createStatement();
            Statement s = conn.createStatement();
            Statement s3 = conn.createStatement();
            rs1 = s1.executeQuery("Select test_Id from  Temp_Class_Test_Info");
            int tid = 0;
            if (rs1.next()) {
                tid = rs1.getInt(1);
            }

            rs3 = s3.executeQuery("select status from Test_And_Set where test_Id = " + tid);
            if (rs3.next()) {
                String geta = rs3.getString(1);
                if (geta.equals("Available")) {
                    rs = s.executeQuery("Select * from  Temp_Class_Test_Info");
                    if (rs.next()) {
            
                classTestBean=new ClassTestBean();
                classTestBean.setTestId(rs.getInt(1));
                classTestBean.setGroupId(rs.getInt(2));
                classTestBean.setStatus(rs.getString(3));
                classTestBean.setTestDate(rs.getTimestamp(4).toString());
                classTestBean.setTestTotalTime(rs.getInt(5));
                classTestBean.setTestName(rs.getString(6));
                classTestBean.setAcademicYear(rs.getString(7));
                classTestBean.setClass_Std(rs.getString(8));
                classTestBean.setDivission(rs.getString(9));
                classTestBean.setSTART_DATE(rs.getString(10));
                classTestBean.setEXPIRE_DATE(rs.getString(11));
                
                return classTestBean;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(JEE.practice.DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(JEE.practice.DBConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return classTestBean;
    }
    public String getCurrentPatternName()
    {
    String CurrentPatternName=null;    
        try {
            
            Connection conn =new DBConnection1().getClientConnection1(Server.getServerIP());
            ResultSet rs;
            PreparedStatement ps;
            String query="SELECT PATTER_NAME FROM PATTERN_STATUS_INFO WHERE STATUS=? ";
            ps=conn.prepareStatement(query);
            ps.setBoolean(1, true);
            rs=ps.executeQuery();
            while(rs.next())
            {
                CurrentPatternName=rs.getString(1);
                return CurrentPatternName; 
            }
            return CurrentPatternName;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    return CurrentPatternName;    
    }
}
