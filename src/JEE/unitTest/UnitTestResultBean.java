/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JEE.unitTest;

/**
 *
 * @author 007
 */
public class UnitTestResultBean {

    private int unitTestId;
    private int totalQuestions;
    private int correctQuestions, wrongQuestions;

    public UnitTestResultBean() {
    }

    public UnitTestResultBean(int unitTestId, int totalQuestions, int correctQuestions, int wrongQuestions) {
        this.unitTestId = unitTestId;
        this.totalQuestions = totalQuestions;
        this.wrongQuestions = wrongQuestions;
        this.correctQuestions = correctQuestions;
    }

    public double getCorrectQuestions() {
        return correctQuestions;
    }

    public void setCorrectQuestions(int correctQuestions) {
        this.correctQuestions = correctQuestions;
    }

    public double getWrongQuestions() {
        return wrongQuestions;
    }

    public void setWrongQuestions(int wrongQuestions) {
        this.wrongQuestions = wrongQuestions;
    }

    public int getUnitTsetId() {
        return unitTestId;
    }

    public void setUnitTestId(int practiceId) {
        this.unitTestId = unitTestId;
    }

    public int getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        this.totalQuestions = totalQuestions;
    }
}